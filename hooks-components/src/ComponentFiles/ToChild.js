import React, { Component } from 'react'

class App extends Component{
    constructor(){
        super();
        this.state = {
            name: "Tushar",
            data: null
        }
        this.setParentData = this.setParentData.bind(this)
    }


    // setParentData = ( users) =>{
    //     this.setState({data: users})
    // }

    setParentData(users){  // If we are using function we should bind {this}
        this.setState({data: users})
    }

    render(){
        console.log(this.state);
        return(
            <div> 
                <h1> Hi {this.state.name}</h1>
                <Compo setParentData={this.setParentData} />
            </div>
        )
    }
}

export default App;


export class Compo extends Component {
    constructor(){
        super();
        this.state = {
            Lname: "Ghayal"
        }
    }

    render() {

        let users = [
            {fname:"Tushar", lname:"Ghayal", age: 26},
            {fname:"Prashant", lname:"Pathak", age: 26},
            {fname:"Vishal", lname:"Hulavle", age: 25},
            {fname:"Piyush", lname:"Barde", age: 28},
        ]

        return (
            <div >
                <button type="button" className="btn btn-primary" onClick={()=>this.props.setParentData(users)}>Send back</button>
            </div>
        )
    }
}