import React, { Component } from 'react';


class App extends Component{

	constructor(){
		super()
		this.state = {
			list: [],
			input:""
		}
	}


	changeHandelar = (e) =>{
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	onAdd = () =>{
		if (this.state.input === "") return

		let list = this.state.list
		let date = new Date().getTime();
		list.push({Todo: this.state.input, id: date, compleated: false})
		this.setState({
			list: list,
			input:""
		})
	}

	remove = (id) =>{
		let list = this.state.list
		let updated = list.filter( item => item.id !== id)

		this.setState({
			list: updated
		})
	}

	complet = (id) =>{
		let list = this.state.list
		let item = list.find( item => item.id === id)
		item.compleated = !item.compleated
		this.setState({
			list: list
		})
	}

	render(){
		return(
			<div className="App">
				<h1>TO List</h1>
				<input name="input"  value={this.state.input} onChange={this.changeHandelar}/>
				<button type="button" className="btn btn-primary" onClick={this.onAdd}>Add</button>
				{
					this.state.list.map(item => (
						<div key={item.id} style={{ cursor: 'pointer', color: item.compleated? 'red' : ''}}>{item.Todo} <span onClick={()=>this.remove(item.id)}>Remove</span> <span onClick={()=>this.complet(item.id)}>Complet</span></div>
					))
				}
			</div>
		)
	}
}

export default App;


import { useState } from 'react';

const App = () =>{
    const [list, setList] = useState([]);
    const [input, setInput] = useState('');

    const onChangeHandeler = (e) =>{
        setInput(e.target.value)
    }

    const add = () =>{
        if (input === '') return
        let date = new Date().getTime()
        setList([...list, {Todo: input, id: date, compleated: false}])
        setInput('')
    }

    const remove = (id) =>{
         let li = list.filter( item => item.id !== id)
        setList(li)
    }
 
    const complet = (id) =>{
        let get = [...list];                                          // we are creating ne reference to that array (creating new array)
        const obj = get.find(item => item.id === id)
        obj.compleated = !obj.compleated
        setList(get)                                                 // we are seting entirely new array
    }

    console.log(list);
    return(
        <div className="text-align">
             <input type="text" className="form-control" name="" value={input} onChange={onChangeHandeler}/>
             <button type="submit" className="btn btn-primary" onClick={add}>Add</button>
             {
                 list.length >= 0 && list.map(item => (
                     <div key={item.id}  style={{ color: item.compleated === true ? 'red' : ''}} >{item.Todo} <span onClick={()=>remove(item.id)}>Remove</span> <span onClick={()=>complet(item.id)}>compleated</span></div>
                 ))
             }
        </div>
    )
}

export default App;