import React from 'react';
import { compose } from 'redux'
import {HOC} from './HOC'
import {HOC2} from './HOC2'

const App = (props) =>{
    console.log(props);
    return(
        <div>
            <h1>App</h1>
        </div>
    )
}

// const enhance = compose(HOC, HOC2)
// export default enhance(App);
            // OR
export default HOC({name:"T"})(HOC2({name:"G"})(App));



import { Component } from 'react'
export const HOC = (props) => (OriginalComponent) => {
    console.log(props);
    class HocComponent extends Component{
        constructor(){
            super()
            this.state = {
                name:"HOC"
            }
        }
        render(){
            return(
                <div style={{border:"10px solid red"}}>
                    <OriginalComponent name={this.state.name} {...this.props}/>
                </div>
            )
        }
    }
    return HocComponent
}



import {Component} from 'react'

export const HOC2 = (props) => (OriginalComponent) =>{
    console.log(props);
    class HocComponent extends Component{
        constructor(){
            super()
            this.state = {
                name:"HOC2"
            }
        }
        render(){
            return(
                <div style={{border: "20px solid"}}>
                    <OriginalComponent name2={this.state.name} {...this.props}/>
                </div>
            )
        }
    }

    return HocComponent;
}

