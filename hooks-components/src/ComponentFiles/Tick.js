import React, { Component } from 'react';

class Tick extends Component {

	constructor(props) {
		super(props);
		this.state = {date: new Date()};
	  }


	  componentDidMount() {
		this.timerID = setInterval(
			() => this.setState({
				date: new Date()
			  }),
			1000
		  );
	}

	componentWillUnmount(){
        clearInterval(this.timerID)
      }
	
	render() {
		return (
			<div data-testid="component-app">
				<h2>It is {this.state.date.toLocaleTimeString()}.</h2>
			</div>
		);
	}
}

export default Tick;



import { useState, useEffect } from 'react';

export default function App(){
	const [ time, setTime ] = useState(new Date())

	useEffect(()=>{
		let interval = setInterval(
			() => setTime(new Date()),
			1000
		  );
		  return () =>{
			  clearInterval(interval)
		  }
	})

	return(
		<h1>{time.toLocaleTimeString()}</h1>
	)
}