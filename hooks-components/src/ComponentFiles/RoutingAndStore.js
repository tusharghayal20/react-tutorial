import React, { lazy, Suspense } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ErrorBoundary from './ErrorBoundary.js'


const LandingPage = lazy(() => import( "./Components/LandingPage/LandingPage"));

<ErrorBoundary>

	<Provider store={store}>

		<Router> {/* <BrowserRouter />  it is return  as <Router /> */}
			 <Switch>  {/* replace by    <Routers />    in router v6 */}

				<Suspense fallback={ <div className="loader_main_container_display"> <img src={'image'} alt="LoaderImageSequence" /></div>}>
					<Route exact path="/" component={LandingPage} />
					<Route exact path="/" component={LandingPage} />

                    {/* Router V6 */}
                    {/* It by default use   exact    */}
                    <Route path='/home' element={<Home/>}/>
				</Suspense>
			</Switch>
		</Router>


	</Provider>

</ErrorBoundary>;

/******************************************************************************** */
/*                   Store
/******************************************************************************** */

import { compose, applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers/index';

import thunk from 'redux-thunk';

const middleware = [thunk];

const initialState = {};

let composeEnhancers = compose(applyMiddleware(...middleware));

const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers
);

export default store;



/******************************************************************************** */
/*                   Action
/******************************************************************************** */
import axios from "axios";

export const create_new_user = (user_data, payment) => async dispatch => {
    try {

      let { data } = await axios.post(`${api}/customers/create`, {data: user_data, payment: payment});
     
      if (data) {
 
      }
    } catch (err) {
      dispatch(setErrors(err));
    }
  };

/******************************************************************************** */
/*                   Reducer
/******************************************************************************** */
const initialState = {
    loader : false,
    success : false,
    user:{},
    logout_user:[],
 
}

export default function ( state = initialState, action ){
    switch ( action.type ){
        case SET_CURRENT_USER:
            return {
                ...state,
                user : action.payload,
                isAuthenticated : !isEmpty( action.payload )
            }

        case LOGOUT:
            return {
                ...state,
                logout_user : action.payload,
                // isAuthenticated : !isEmpty( action.payload )
            }
        default :
            return state
    }
}

/***************************************** */
import { combineReducers } from 'redux';

export default combineReducers({
    errors : errorReducer,
    auth: authReducer,
})

/******************************************************************************** */
/*                   Type
/******************************************************************************** */
/******************************************************************************** */
/*                   Type
/******************************************************************************** */
/*
//Reract
                useState Hook
                useEffect Hook
                useRef Hook
                useMemo Hook
                useContext Hook

                useCallback Hook
                useReducer Hook



                useSelector()
                useDispatch()
                const store = useStore()

                useHistory.
                useParams.
                useLocation.
                useRouteMatch.

*/