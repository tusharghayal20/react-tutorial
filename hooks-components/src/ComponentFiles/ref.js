import React, {Component} from 'react';

class App extends Component{
     constructor(props) {
        super(props);
        this.setState({
            htmF: false
        })
        this.textInput = React.createRef();
    }

    componentDidMount(){
        console.log(this.textInput);
        this.textInput.current.focus();
    }

    focudRef = () =>{
        this.textInput.current.focus();
    }

    focudHtml = () =>{
        this.setState({
            htmF: true
        }) 
    }

    render(){
        return(
            <div>
                 <input type="text" ref={this.textInput}/>
                 <button type="button" classNmae="btn btn-primary" onClick={this.focudRef}>Fouc ref</button>
                 <input type="text" autoFocus={this.state.htmF}/>
                 <button type="button" classNmae="btn btn-primary" onClick={this.focudHtml}>Fouc Html</button>
            </div>
        )
    }
}

export default App;