import React, { useEffect, useRef } from 'react'

const FocusInput = () => {

    const inputRef = useRef(null)

    useEffect(() => {
        inputRef.current.focus()
    }, [])

    return (
        <div className="row mt-5 justify-content-md-center">
            <div class="col-md-3">
              <input type="text" class="form-control" ref={inputRef}/>
              {/* // here ref attribute is reserved */}
            </div>
        </div>
    )
}

export default FocusInput
