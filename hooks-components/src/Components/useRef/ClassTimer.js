import React, { Component } from 'react'

export class ClassTimer extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             timer: 0
        }
    }

    componentDidMount(){
        this.interval = setInterval(()=>{
            this.setState({timer : this.state.timer + 1})
        },1000)
    }


    componentWillUnmount(){
        clearInterval(this.interval)
    }
    
    render() {
        return (
            <div>
                <h1 className="ml-5">
                    Class Timer : {this.state.timer}
                </h1>
                <div className="ml-5 pl-5"><button type="button" onClick={()=>clearInterval(this.interval)} class="btn btn-primary">Stop</button></div>
            </div>
        )
    }
}

export default ClassTimer
