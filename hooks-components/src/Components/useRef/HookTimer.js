import React, { useState, useRef, useEffect  } from 'react'

function HookTimer() {
    const [timer, setTimer ] = useState(0)
    const intervelRef = useRef()
    useEffect(() => {
        intervelRef.current = setInterval(() => {
            setTimer(preTime => preTime + 1)
        }, 1000);
        return () => {
            clearInterval(intervelRef.current)
        }
    }, [])

    return (
        <div>
            <h1 className="ml-5">
                HookTimer : {timer} 
            </h1>
            <div className="ml-5 pl-5"><button type="button" onClick={()=>clearInterval(intervelRef.current)} class="btn btn-primary">Stop</button></div>
        </div>
    )
}

export default HookTimer
