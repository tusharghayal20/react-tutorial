import React, { Component } from 'react'

export class ResizedWindow extends Component {

   
  componentDidMount() {
    this.updateDimensions()
  }

  componentDidUpdate() {
    window.addEventListener('resize', this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
  }

  updateDimensions = () => {
    this.setState({width: window.innerWidth, height: window.innerHeight})
  }

    render() {
        console.log("============>", this.state);
        return (
            <div>
               <h1>{this?.state?.width} x {this?.state?.height}</h1>
            </div>
        )
    }
}

export default ResizedWindow
