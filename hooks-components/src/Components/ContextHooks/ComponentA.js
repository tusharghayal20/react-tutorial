import React from 'react';
import ComponentB from './ComponentE';

function ComponentA() {
	return <ComponentB />;
}

export default ComponentA;
