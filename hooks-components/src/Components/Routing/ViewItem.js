import React, { useEffect, useState } from 'react'
import { BrowserRouter, Routes, Route, Link, useLocation, useParams, useNavigate  } from 'react-router-dom';


const ViewItem = (props) => {

    // const param = useParams();
    // console.log(param.id);
    const [post, setPost] = useState()
    const {id} = useParams();
    
    useEffect(() => {
        fetchData()
    }, [])

    const fetchData = async () =>{
        const data = await(await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)).json()
        setPost(data)
    }

    return (
        <div>
            <h1>{post && post.title}</h1>
            <p>{post && post.body}</p>
        </div>
    )
}

export default ViewItem
