import React from 'react'
import { BrowserRouter, Routes, Route, Link, useLocation, useParams, useNavigate  } from 'react-router-dom';


import Home from './Home'
import About from './About'
import Contact from './Contact'
import Shop from './Shop'
import ViewItem from './ViewItem'
import NoMatch from './NoMatch.js'
import Nave from './Nave'

const index = () => {
    return (
        <>
            <Nave />

            {/*  BrowserRouter is wrapes around the main App comp */}
            {/* <BrowserRouter> */}
                <Routes>
                    <Route exact path='/' element={<Home/>}/>
                    <Route exact path='/home' element={<Home/>}/>
                    <Route exact path='/about' element={<About />}/>
                    <Route exact path='/contact' element={<Contact />}/>
                    <Route exact path='/shop' element={<Shop />}/>
                    <Route exact path='/shop/post/:id' element={<ViewItem />}/>
                    <Route path="*" element={<NoMatch />}/>
                </Routes>
            {/* </BrowserRouter> */}
           
        </>
    )
}

export default index
