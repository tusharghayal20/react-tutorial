import React from 'react'
import { BrowserRouter, Routes, Route, Link, useLocation, useParams, useNavigate  } from 'react-router-dom';

const Nave = () => {
    const location = useLocation ()
    const params = useParams ()


    console.log(location);
    console.log(params);
    return (
            <>
            <ul className="nav nav-tabs">
                <li className="nav-item"><Link to='/home' className="nav-link active">Home</Link></li>
                <li className="nav-item"><Link to='/about' className="nav-link active">About</Link></li>
                <li className="nav-item"><Link to='/shop' className="nav-link active">Shop</Link></li>
                <li className="nav-item"><Link to='/contact' className="nav-link active">Contact</Link></li>
            </ul>
        </> 
    )
}

export default Nave
