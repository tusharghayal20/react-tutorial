import React, { useEffect, useState } from 'react'
import { BrowserRouter, Routes, Route, Link, useLocation, useParams, useNavigate  } from 'react-router-dom';


const Shop = () => {

    const [posts, sePposts] = useState([])

    useEffect(()=>{
        fetchSata()
    },[])

    const fetchSata = async () => {
        // const res = await (await fetch('https://jsonplaceholder.typicode.com/posts')).json()
        // console.log(res);
        // sePposts(res)
        // Or
        const res = await fetch('https://jsonplaceholder.typicode.com/posts')
        const data = await res.json()
        // console.log(data);
        sePposts(data);
    }

    return (
        <div>
            {
               posts && posts.map( (post)=>(
                <div className="card text-left" key={post.id}>
                    <Link to={`post/${post.id}`}>
                        <div className="card-body">
                            <h4 className="card-title">{post.title}</h4>
                            <p className="card-text">{post.body}</p>
                        </div>
                    </Link>
                </div>
               ))
            }
        </div>
    )
}

export default Shop
