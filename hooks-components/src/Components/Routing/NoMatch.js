import React from 'react'

const NoMatch = () => {
    return (
        <h1 className={{"trxtAlign":"center"}}>
            No Match Found
        </h1>
    )
}

export default NoMatch
