//*********************             Store.js        ************************* */
import { compose, applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers/index';
import thunk from 'redux-thunk';
const middleware = [thunk];

const initialState = {};

let composeEnhancers = compose(applyMiddleware(...middleware));

const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers
    );
    
    export default store;
    
//*********************             Reducer.js        ************************* */
import { combineReducers } from 'redux';

export default combineReducers({
    errors : errorReducer,
    auth: authReducer,
    posts : postReducer,
    aws: awsReducer,
    comodity: comodityReducer,
})

//*********************             App.js        ************************* */
import React ,{lazy, Suspense} from "react";
import { Provider, connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch, withRouter } from "react-router-dom";
import store from "./store/store";
import ErrorBoundary from './utils/ErrorBoundary'


const App = props => {
    return (
      <ErrorBoundary>
      
        <Provider store={store}>


                <Router>
                    <Switch>
                        <Suspense fallback={ <div className='loader_main_container_display'> <img src={loaderimage} alt="LoaderImageSequence" /> </div>}>
                            <Route exact path="/" component={RedirectCompo} />
                        </Suspense>
                    </Switch>
                </Router>


        </Provider>
      
      </ErrorBoundary>
    );
};


/***************************************** */

const mapStateToProps = (state) =>({
  auth: state.auth,
  error: state.error,
})

const mapDispatchToProps = (dispatch) => ({
  action: () => dispatch(action())
 })

/*
const mapDispatchToProps = {
  create_new_user,
}
  
*/
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(App));
  


//*********************             Reducer.js        ************************* */

import { SET_LOADER, CLEAR_LOADER, SET_SUCCESS, CLEAR_SUCCESS, ORGANIZATION, SET_CURRENT_USER, URL_PATH, SET_GRAPH_LOADER, CLEAR_GRAPH_LOADER, LOGOUT} from "../types";
import isEmpty from '../../utils/isEmpty';

const initialState = {
    loader : false,
    success : false,
    user:{},
    isAuthenticated:false,
    graph_loader: false,
    logout_user:[],
}

export default function ( state = initialState, action ){
    switch ( action.type ){
        case SET_CURRENT_USER:
            return {
                ...state,
                user : action.payload,
                isAuthenticated : !isEmpty( action.payload )
            }

        case LOGOUT:
            return {
                ...state,
                logout_user : action.payload,
                // isAuthenticated : !isEmpty( action.payload )
            }
        default :
            return state
    }
}

  //*********************             Action.js        ************************* */
  export const create_new_user = (user_data, payment) => async dispatch => {
    try {

      let { data } = await axios.post(`${solefront}/customers/create`, {
        data: user_data,
        payment: payment
      });

      if (data) {
        dispatch({ type: CLEAR_LOADER });
        dispatch({ type: SET_SUCCESS });
      }
    } catch (err) {
      dispatch(setErrors(err));
      dispatch({ type: CLEAR_LOADER });
    }
  };

  


  //*********************             Map to props/dispach.js        ************************* */

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors,
  });
  
  const mapDispatchToProps = (dispatch) => ({
    action: () => dispatch(action())
   })

  export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SearchResult));

//Reract
useState Hook
useEffect Hook
useRef Hook
useMemo Hook
useContext Hook

useCallback Hook
useReducer Hook



useSelector()
useDispatch()
const store = useStore()

useHistory.
useParams.
useLocation.
useRouteMatch.