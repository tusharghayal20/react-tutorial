import React, {Component} from 'react'
                //Accept original component
const HocAddBorder = (OriginalComponent) =>{
    const NewComponent = () => {
            return (
                <>
                    <div>Header</div>
                    <div style={{backgroundColor: "lightblue"}}>
                        <OriginalComponent/>
                    </div>
                    <div>Footer</div>
                </>
            )
    }
    return NewComponent
}

export default HocAddBorder;//Updated component

// import React, {Component} from 'react'
//                 //Accept original component
// const HocAddBorder = (OriginalComponent) =>{
//     class NewComponent extends Component{
//         render(){
//             return (
//                 <>
//                     <div>Header</div>
//                     <div style={{backgroundColor: "lightblue"}}>
//                         <OriginalComponent/>
//                     </div>
//                     <div>Footer</div>
//                 </>
//             )
//         }
//     }
//     return NewComponent
// }

// export default HocAddBorder;//Updated component