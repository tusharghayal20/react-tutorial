// import React from 'react'
// import HocComponent from './HocComponent'

// const ClickCounter = props => <><button onClick={props.incermentCouter}> Click {props.counter} times</button>{props.name}</>
// export default HocComponent(ClickCounter)

/*
const ClickCounter = HocComponent(props => <><button onClick={props.incermentCouter}> Click {props.counter} times</button>{props.name}</>)
export default ClickCounter
*/


/************************** Different types of export  ***************************/

// export default HocComponent(function ClickCounter(props) {
//     return (
//     <><button onClick={props.incermentCouter}> Click {props.counter} times</button>{props.name}</>
//     )
// })



// export const ClickCounter = HocComponent((props) =><><button onClick={props.incermentCouter}> Click {props.counter} times</button>{props.name}</>)

import React, { Component } from 'react'
import HocComponent from './HocComponent'
class ClickCounter extends Component {
    // constructor(props){
    //     super(props)
    //     this.state = {
    //         counter: 0
    //     }
    // }

    // incermentCouter = () =>{
    //     this.setState((prevState)=>({
    //         counter: prevState.counter + 1
    //     }))
    // }

    handelerClick = (operator) =>{
        this.setState((prew)=>({
            count : operator === '+' ? prew.count + 1 : prew.count - 1
        }))
        //OR
        this.setState((prew)=>{
            return {count : operator === '+' ? prew.count + 1 : prew.count - 1}
        })
    }

    render() {
        return (
            <>
                <div>
                    <button onClick={this.props.incermentCouter}> Click {this.props.counter} times</button>{this.props.name}
                </div>
                <h1 >Hi</h1>
            </>
        )
    }
}

export default HocComponent(ClickCounter, 10) // 10 as parameter to hoc
