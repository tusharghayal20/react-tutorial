import React, { Component, compose  } from 'react'
import HocComponent from './HocComponent'
import HocAddBorder from './HocAddBorder'
class HoverCounter extends Component {

    // constructor(props){
    //     super()
    //     this.state = {
    //         counter: 0
    //     }
    // }

    // incermentCouter = () =>{
    //     this.setState((prevState)=>({
    //         counter: prevState.counter + 1
    //     }))
    // }

    render(){
        return(
            <div><h1 onMouseOver={this.props.incermentCouter}> {this.props.name} Click {this.props.counter} times</h1></div>
        )
    }
}

export default HocAddBorder(HocComponent(HoverCounter, 5)) // 5 as paramater to hoc