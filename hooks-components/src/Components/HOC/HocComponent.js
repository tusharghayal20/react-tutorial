import React, {Component} from 'react'

const HocComponent = (OriginalComponent, increment )=>{
    class NewComponent extends Component{
        constructor(props){
            super(props)
            this.state = {
                counter: 0
            }
            console.log(this.props);
        }
        
        incermentCouter = () =>{
            this.setState((prevState)=>({
                counter: prevState.counter + increment
            }))
        }
        
        render(){
            return <OriginalComponent name="Tushar" incermentCouter ={this.incermentCouter} {...this.state}/>
        }
    }
    return NewComponent
}

export default HocComponent;//Updated component