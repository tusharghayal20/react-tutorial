import React from 'react'
import ClickCounter from './ClickCounter'
import HoverCounter from './HoverCounter'

const index = () => {
    return (
        <div>
            {/* <ClickCounter /> */}
            <HoverCounter />
        </div>
    )
}

export default index
