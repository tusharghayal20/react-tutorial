import React, { useState } from 'react'

const UserForm = () => {
    
    const [ firstName, setFirstName ] = useState('')
    const [ lastName, setLastName ] = useState('')

    const submitHandeler = (e) =>{
        e.preventDefault()
        alert(`Hellow ${firstName} ${lastName}`)
    }

    return (
       <div className="row my-5 justify-content-md-center">
            <form onSubmit={submitHandeler}>
                <div className="form-group">
                  <label htmlFor="">First name</label>
                  <input type="text" className="form-control" name={firstName} value={firstName} onChange={e => setFirstName(e.target.value)}  aria-describedby="helpId" placeholder=""/>
                </div>
                <div className="form-group">
                  <label htmlFor="">Last Name</label>
                  <input type="text" className="form-control" name={lastName} value={lastName}  onChange={e => setLastName(e.target.value)} aria-describedby="helpId" placeholder=""/>
                </div>
            <button type="submit" name="" id="" className="btn btn-primary">Submit</button>
            </form>
       </div>
    )
}

export default UserForm
