import React, { useEffect, useState } from 'react'
import useDocumentTitle from '../Hooks/useDocumentTitle'

const DockTitleOne = () => {
    const [count, setCount] = useState(0)

    // useEffect(() => {
    //     document.title = `Count ${count}`
    //     return () => {
           
    //     }
    // }, [count])

    useDocumentTitle(count)

    return (
        <div className="row mt-5 justify-content-md-center">
            <button type="button" onClick={()=>setCount(preCount=> preCount + 1)}> Count {count} </button>
        </div>
    )
}

export default DockTitleOne
