import React, { useEffect, useState } from 'react'
// import useDocumentTitle from './Hooks/useDocumentTitle'

const DockTitleTwo = () => {
    const [count, setCount] = useState(0)

    // useEffect(() => {
    //     document.title = `Count ${count}`
    //     return () => {
           
    //     }
    // }, [count])

    function useDocumentTitle(count) {

        useEffect(() => {
            document.title = `Count ${count}`  // It also works 
            return () => {
               
            }
        }, [count])
        
        
    }


    useDocumentTitle(count)

    return (
        <div className="row mt-5 justify-content-md-center">
            <button type="button" onClick={()=>setCount(preCount=> preCount + 1)}> Count {count} </button>
        </div>
    )
}

export default DockTitleTwo
