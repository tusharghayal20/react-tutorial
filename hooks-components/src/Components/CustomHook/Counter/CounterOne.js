import React, { useState } from 'react'
import useCounter from '../Hooks/useCounter'

const CounterOne = () => {

    /*
    const [ count, setCount ] = useState(0)

    const increment = () =>{
        setCount( prevCount => prevCount + 1 )
    }

    const decrement = () =>{
        setCount( prevCount => prevCount - 1 )
    }

    const reset = () =>{
        setCount( 0 )

          handelerClick = (operator) =>{
        this.setState((prew)=>({
            count : operator === '+' ? prew.count + 1 : prew.count - 1
        }))
        //OR
        this.setState((prew)=>{
            return {count : operator === '+' ? prew.count + 1 : prew.count - 1}
        })
    }
    }*/

    //custom hooks
    const [count, increment, decrement, reset] = useCounter()
    // const [count, increment, decrement, reset] = useCounter(10, 10)


    return(
        <>
               <h1> Custom hooks</h1>
            <div className="row mt-5 justify-content-md-center">
                <div className='d-flex'>
                <h1>Count : {count}</h1>
                </div>
                <div className='d-flex'>
                    <button type="button" className="btn btn-outline-danger m-5" onClick={decrement}>Decrese</button>
                    <button type="button" className="btn btn-outline-primary m-5" onClick={reset}>Reset</button>
                    <button type="button" className="btn btn-outline-success m-5" onClick={increment}>Increse</button>
                </div>
            </div>
        </>
    )
}

export default CounterOne
