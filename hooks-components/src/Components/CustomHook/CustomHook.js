import React from 'react'
import DockTitleTwo from './DockTitle/DockTitleOne'
import DockTitleOne from './DockTitle/DockTitleOne'
import CounterOne from './Counter/CounterOne'
import CounterTwo from './Counter/CounterTwo'
import UserForm from './UserForm/UserForm'

const CustomHook = () => {
    return (
        <>
            <DockTitleOne />
            <DockTitleTwo />
            <CounterOne />
            <CounterTwo />
            <UserForm />
        </>
    )
}

export default CustomHook

