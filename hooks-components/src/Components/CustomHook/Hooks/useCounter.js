import { useState } from 'react'

const useCounter = (initialCount = 0, multiple = 1) => {

    const [ count, setCount ] = useState(initialCount)

    const increment = () =>{
        setCount( prevCount => prevCount + multiple )
    }

    const decrement = () =>{
        setCount( prevCount => prevCount - multiple )
    }

    const reset = () =>{
        setCount( initialCount )
    }

    return [count, increment, decrement, reset]
}

export default useCounter
