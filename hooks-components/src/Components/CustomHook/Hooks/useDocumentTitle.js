import { useEffect } from 'react'

const useDocumentTitle = (count) => {

    useEffect(() => {
        document.title = `Count ${count}`
        return () => {
           
        }
    }, [count])

    // return (  //Don't need jxs
    //     <div>
            
    //     </div>
    // )
}

export default useDocumentTitle

