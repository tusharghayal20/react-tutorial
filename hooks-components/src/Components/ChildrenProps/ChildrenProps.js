import React from 'react'

const ChildrenProps = (props) => {
    console.log(props.children);
    return (
        <div>
            {props.children}
        </div>
    )
}

export default ChildrenProps
