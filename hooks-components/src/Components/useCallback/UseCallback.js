import React from 'react'
import ParrentComponent from './ParrentComponent'

const UseCallback = () => {
    return (
        <div>
            <ParrentComponent />
        </div>
    )
}

export default UseCallback
