import React from 'react'

const Button = ({handleClick, children}) => {
    console.log("Button", children);
    return (
        <div>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>handleClick('-')}>Decrement</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>handleClick('+')}>Increment</button>
        </div>
    )
}

export default React.memo(Button)
