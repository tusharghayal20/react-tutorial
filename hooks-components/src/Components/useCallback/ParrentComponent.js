import React, { useState, useCallback } from 'react'
import Button from './Button'
import Count from './Count'
import Title from './Title'
// https://youtu.be/IL82CzlaCys?list=PLC3y8-rFHvwisvxhZ135pogtX7_Oe3Q3A
const ParrentComponent = () => {

    const [age, setAge] = useState(26)
    const [salary, setSalary] = useState(50000)

    // //Normal function
    // const ageAction = (action) =>{
    //     if (action === "+") {
    //         setAge( preAge => preAge + 1)
    //     } else {
    //         setAge( preAge => preAge - 1)
    //     }
    // } // Render age count & button


    //There is the diff betmeen two function // Check log on both

    //Use this one
    const ageAction = useCallback((action) =>{
        if (action === "+") {
            setAge( preAge => preAge + 1)
        } else {
            setAge( preAge => preAge - 1)
        }
    },[]) // Render only age count

    // const ageAction = useCallback((action) =>{
    //     if (action === "+") {
    //         setAge( age + 1)
    //     } else {
    //         setAge( age - 1)
    //     }
    // },[age]) // Render age count & button

    const salaryAction = useCallback((action) =>{
        if (action === "+") {
            setSalary(preSalary => preSalary + 1000)
        } else {
            setSalary(preSalary => preSalary - 1000)
        }
    },[])

    return (
        <div>
            <Title />
            <Count text="Age" count={age}/>
            <Button handleClick={ageAction}>Age</Button>
            <Count text="Salary" count={salary}/>
            <Button handleClick={salaryAction}>Salary</Button>
        </div>
    )
}

export default ParrentComponent
