import React, { Component } from 'react';

export class index extends Component {
	constructor() {
		console.log(
			'=====================constructor================================='
		);
		super();
		console.count('constructor');
		this.state = {
			com: 'constructor',
			display: true,
		};
	}

	static getDerivedStateFromProps(props, state) {
		console.log(
			'=====================getDerivedStateFromProps================================='
		);
		console.count('getDerivedStateFromProps');

		console.log(props);
		console.log('state', state);
		if (props.name !== '') {
			return {
				name: props.name,
			};
		}
	}

	// shouldComponentUpdate(nextProps, nextState){
	//     console.count('shouldComponentUpdate')
	//     return false
	// }

	setCom = () => {
		console.log('setCom');
		this.setState({
			com: 'setCom',
		});
	};

	getSnapshotBeforeUpdate(prevProps, prevState) {
		console.count('getSnapshotBeforeUpdate');
		console.log(prevState);
		return null;
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		console.count('componentDidUpdate');
	}

	componentDidMount(prevProps, prevState, snapshot) {
		console.count('componentDidMount');
		fetch('https://jsonplaceholder.typicode.com/posts')
			.then((response) => response.json())
			.then((json) =>
				this.setState({
					json: json,
				})
			);
	}

	hide = () => {
		this.setState({
			display: !this.state.display,
		});
	};

	// componentWillUnmount() {
	// 	console.log('componentWillUnmount');
	// }

	render() {
		console.count('render');
		console.log('======>', this.state);
		return (
			<div>
                LifeCycl e <h1>{this.state.com}</h1>
				{this.state.display ? (
					<div>
                        <Unmount />
					</div>
				) : null}
				<button type="button" onClick={this.setCom} className="btn btn-primary">
					Set state
				</button>
				<button type="button" onClick={this.hide} className="btn btn-primary">
					hide
				</button>
			</div>
		);
	}
}

export default index;

export class Unmount extends Component {
    componentDidMount(prevProps, prevState, snapshot) {
		console.count('componentDidMount');
	}
    componentWillUnmount() {
		console.log('componentWillUnmount');
	}
	render() {
		return <div>Unmount</div>;
	}
}
