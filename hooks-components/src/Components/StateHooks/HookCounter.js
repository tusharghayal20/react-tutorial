import React, { useState } from 'react'
function HookCounter(){

    const [ count, setCount ] = useState(0)
    const [ count1, setCount1 ] = useState(0)
    const [ count2, setCount2 ] = useState(0)
    
    const [ profile, setProfile ] = useState({ fname:'', lname:'' })
    const initialCount = 0;
    
    const incrementFive = () =>{
        for (let index = 1; index <= 5; index++) {
            setCount( prevCount => prevCount + 1 )
        }
    }
    
    const decrementFive = () =>{
        for (let index = 1; index <= 5; index++) {
            setCount( prevCount => prevCount - 1 )
        }
    }

    
    const [ createArray, setCreateArray ] = useState([])

    const addItem = () =>{
        setCreateArray([
            ...createArray,
            {
            id: createArray.length,
            value: Math.floor(Math.random() * 10) + 1
        }])
    }
    
    return(
        <div>
            <div className='d-flex'>
                <div onClick={ ()=>setCount1( count1 + 1) }>{count1}</div>
                <div onClick={ ()=>setCount2( prevCount => prevCount + 1) }>{count2}</div>
            </div>
            <div className='d-flex'>
                <span class="badge badge-primary" onClick={()=>setCount( prevCount => prevCount + 1 )}>Increse</span>
                <div>{count}</div>
                <span class="badge badge-primary" onClick={()=>setCount( prevCount => prevCount - 1 )}>Decrese</span>
            </div>
            <div className='d-flex'>
                <span class="badge badge-primary" onClick={incrementFive}>Increse 5</span>
                <span class="badge badge-primary" onClick={()=>setCount( initialCount )}>Reset</span>
                <span class="badge badge-primary" onClick={decrementFive}>Decrese 5</span>
            </div>

            <div className='d-flex'>
                  <input type="text" class="form-control" name="" value={profile.fname} onChange={e =>setProfile({...profile, fname: e.target.value })} id="" aria-describedby="helpId" placeholder="" />
                  <input type="text" class="form-control" name="" value={profile.lname} onChange={e =>setProfile({...profile, lname: e.target.value })} id="" aria-describedby="helpId" placeholder="" />
            </div>

            <div className='d-flex justify-content-between'>
                <div>{profile.fname}</div>
                <div>{profile.lname}</div>
            </div>
            <div>{JSON.stringify(profile)}</div>

            <button type="button" name="" id="" class="btn btn-primary" onClick={addItem}>Add Item</button>
            <ul>
                {
                   createArray && createArray.map( item => (
                    <li key={item.id}>{item.value}</li>
                    ))
                }
            </ul>
        </div>
    )
}

export default HookCounter