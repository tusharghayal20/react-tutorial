import React, { Component } from 'react';

class ClassCounter extends Component {
    constructor(props){
        super(props);
        this.state = {
            count1: 0,
            count2: 0,
        }
    }

    incrementCount1 = () =>{
        this.setState({
            count1: this.state.count1 + 1
        })
    }

    incrementCount2 = () =>{
        this.setState((prevState)=>({
            count2: prevState.count2 + 1
        }))
    }

    handlear = () =>{
        this.setState((prevState, props)=>({
            count2: prevState.count2 + 1
        }))
    }

    someHandelar = () => {
        this.setState(prevState => {
          return {
            count: prevState.count + 1
          }
        })
      }

	render() {
		return <>
                <div onClick={this.incrementCount1}>{this.state.count1}</div>
                <div onClick={this.incrementCount2}>{this.state.count2}</div>
            </>
	}
}

export default ClassCounter;
