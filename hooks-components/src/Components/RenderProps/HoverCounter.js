import React from 'react'

function HoverCounter({count, incrementCount, decremantCount}) {
    return (
        <>
            <div>{count}</div>
            <button onMouseOver={incrementCount} ClassName="btn btn-primary">onMouseOver</button>
            <button onMouseOut={decremantCount} ClassName="btn btn-primary">onMouseOut</button>
        </>
    )
}

export default HoverCounter
