import React, { Component } from 'react'

class Counter extends Component {

    constructor(props){
        super(props)
        this.state = {
            count: 0
        }
    }

    incremantCount = () =>{
        this.setState((prevState)=>{
            return { count: prevState.count + 1}
        })
    }

    decremantCount = () =>{
        this.setState((prevState)=>{
            return { count: prevState.count - 1}
        })
    }


    handelerClick = (operator) =>{
        this.setState((prew)=>({
            count : operator === '+' ? prew.count + 1 : prew.count - 1
        }))
        //OR
        this.setState((prew)=>{
            return {count : operator === '+' ? prew.count + 1 : prew.count - 1}
        })
    }
    
    render() {
        return (
            <div>
                {this.props.render(this.state.count, this.incremantCount, this.decremantCount)}
            </div>
        )
    }
}

export default Counter
