import React from 'react'
import User from './User'
import Counter from './Counter'
import ClickCounter from './ClickCounter'
import HoverCounter from './HoverCounter'

// The term “render prop” refers to a technique for sharing code between React components using a prop whose value is a function.
// A component with a render prop takes a function that returns a React element and calls it instead of implementing its own render logic.

function index() {
    return (
        <div>
            <h2>Render Props</h2>
            {/* <User name={"Tushar"}/> */}
            {/* <User name={()=> 'Tushar' }/> */}
            {/* <User name={(isLoggedIn) => isLoggedIn ? 'Tushar' : 'Guest' }/> */}
            <User render={(isLoggedIn) => isLoggedIn ? 'Tushar' : 'Guest' }/>


            <Counter render={(count, incrementCount, decremantCount)=>(
                <ClickCounter count={ count }  incrementCount={ incrementCount } decremantCount={decremantCount}/>
            )}/>

            <Counter render={(count, incrementCount, decremantCount)=>(
                <HoverCounter count={ count }  incrementCount={ incrementCount } decremantCount={decremantCount}/>
            )}/>

        </div>
    )
}

export default index
