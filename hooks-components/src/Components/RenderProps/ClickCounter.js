import React from 'react'

function ClickCounter({count, incrementCount, decremantCount}) {
    return (
        <>
            <div>{count}</div>
            <button onClick={incrementCount} class="btn btn-primary">Increment</button>
            <button onClick={decremantCount} class="btn btn-primary">Decremant</button>
        </>
    )
}

export default ClickCounter
