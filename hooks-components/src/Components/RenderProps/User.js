import React from 'react'

function User(props){
    console.log(props);
    return(
        //We are callinh this function here and passing parameter frome here
        //After calling the function will get called from index 
        // <h1>{props.name(true)}</h1>
        <h1>{props.render(true)}</h1>
    )
}

export default User;