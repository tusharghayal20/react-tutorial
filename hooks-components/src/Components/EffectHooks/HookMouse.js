import React, { useState, useEffect } from 'react';

function HookMouse() {
	const [Position, setPosition] = useState({ x: 0, y: 0 });

	const logMousePosition = (e) => {
		setPosition({
			x: e.clientX,
			y: e.clientY,
		});
	};

	useEffect(() => {
		document.addEventListener('mousemove', logMousePosition);

        //Unmount in hooks
		return () => {
			console.log('Component unmounting code');
			window.removeEventListener('mousemove', logMousePosition);
		};
	}, []);

	return (
		<div>
			X - {Position.x} Y - {Position.y}
		</div>
	);
}

export default HookMouse;
