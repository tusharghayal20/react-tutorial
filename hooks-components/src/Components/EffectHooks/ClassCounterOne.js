import React, { Component } from 'react'

class ClassCounterOne extends Component{

    constructor(props){
        super(props);
        this.state = {
            count: 0
        }
    }

    componentDidMount(){
        document.title = this.state.count
    }

    componentDidUpdate(){
        document.title = this.state.count
    }

    render(){
        return(
            <div>
                <button type="button" onClick={()=>this.setState((preProps)=>({count: preProps.count + 1 })) } className="btn btn-primary">{this.state.count}</button>
            </div>
        )
    }
}

export default ClassCounterOne;