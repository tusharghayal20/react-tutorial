import React, { Component } from 'react';

class IntervalClassCount extends Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 0,
		};
	}

	componentDidMount() {
		this.innterval = setInterval(this.tick, 1000);
	}

	componentWillUnmount() {
		//Don't figure out wgy we requir this unmount
		clearInterval(this.innterval);
	}

	tick = () => {
		this.setState((preState) => ({
			count: preState.count + 1,
		}));
	};

	render() {
		return <div>{this.state.count}</div>;
	}
}

export default IntervalClassCount;
