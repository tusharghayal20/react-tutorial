import React, { useState, useEffect } from 'react'

const HookCounterOne = () =>{
    const [ count, setCount ] = useState(0);
    const [ name, setName ] = useState('');
    
    useEffect(()=>{
        document.title = count
    },[count])

    return(
        <div>
            <input type="text" onChange={e => setName(e.target.value)} value={name} className="form-control" aria-describedby="helpId"/>
            <button type="button" onClick={()=>setCount( preount => preount  + 1) } className="btn btn-primary">{count}</button>
        </div>
    )
}

export default HookCounterOne;