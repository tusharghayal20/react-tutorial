import React, { useState, useEffect } from 'react';
import axios from 'axios';

const DataFetching = () => {
	const [posts, setPosts] = useState({});
	const [post, setPost] = useState({});
	const [id, setId] = useState();

	// There is one last catch. In the code, we are using async/await to fetch data from a third-party API. According to the documentation every function annotated with async returns an implicit promise: "The async function declaration defines an asynchronous function, which returns an AsyncFunction object. An asynchronous function is a function which operates asynchronously via the event loop, using an implicit Promise to return its result. ".
	// However, an effect hook should return nothing or a clean up function. That's why you may see the following warning in your developer console log: 07:41:22.910 index.js:1452 Warning: useEffect function must return a cleanup function or nothing. Promises and useEffect(async () => ...) are not supported, but you can call an async function inside an effect.. That's why using async directly in the useEffect function isn't allowed.

	//   **  Will show Warning  **
	// useEffect(async () => {
	//     const { data  } = await axios('https://jsonplaceholder.typicode.com/posts')
	//     console.log(data);
	//     setPosts(data)
	//   },[]);

	//      ||
	//     \||/
	//      \/

	// Correct way  // But don't know why throwing error
	// an effect hook should return nothing or a clean up function.
	// useEffect(()=>{
	//     const fetchData = async () =>{
	//         const { data } = await axios('https://jsonplaceholder.typicode.com/posts')
	//         // console.log(data);
	//         setPosts(data)
	//     }
	//     fetchData()
	// },[])

	const onclickHandlear = async (id) => {
		const { data } = await axios(
			`https://jsonplaceholder.typicode.com/posts/${id}`
		);
		setPost(data);
	};

	//Another method
	useEffect(() => {
		axios.get('https://jsonplaceholder.typicode.com/posts').then((res) => {
			// console.log(res.data);
			setPosts(res.data);
		});
	}, []);
	console.log(posts);
	console.log(post);

	return (
		<>
			<div>
				{posts.length > 0
					? posts.map((post, index) => (
							<div key={index} onClick={() => onclickHandlear(post.id)}>
								{post.title}
								<div> {post && post.body}</div>
							</div>
					  ))
					: null}
			</div>
		</>
	);
};

export default DataFetching;
