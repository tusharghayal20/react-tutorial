import React, { useState, useEffect } from 'react';

const IntervalHookCount = () => {
	const [count, setCount] = useState(0);

	//With dippendency // this one is the perfect one 
	// It will not reRender the useEffect again and again
	
	const tick = () => {
		setCount((preCount) => preCount + 1);
	};

	useEffect(() => {
		const interval = setInterval(tick, 1000);
		console.log("useEffect");
		//component unmount
		return () => {
			clearInterval(interval);
		};
	}, []); //<================================================

	//Without dippendency
	// It will not reRender the useEffect again and again
	
/*
	const tick = () => {
		setCount(count + 1);
	};

	useEffect(() => {
		const interval = setInterval(tick, 1000);
		console.log("useEffect");
		//component unmount
		return () => {
			clearInterval(interval);
		};
	}, [count]); //<================================================ To remove this warning use below code

	// const tick = () => {
	// 	setCount(count + 1);
	// };

/*
	useEffect(() => {

		const tick = () => {
			setCount(count + 1);
		};
		console.log("useEffect");
		const interval = setInterval(()=>tick(), 1000);
		//component unmount
		return () => {
			clearInterval(interval);
		};
	}, [count]); //<================================================
*/
	return <div>{count}</div>;
};

export default IntervalHookCount;
