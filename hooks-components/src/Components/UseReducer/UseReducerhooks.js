import React from 'react'
// import CounterOne from './CounterOne'
// import CounterTwo from './CounterTwo'
// import CounterThree from './CounterThree'
// import DataFetchingOne from './DataFetchingOne'
// import DataFetchingTwo from './DataFetchingTwo'
import ComponentABC from './ComponentABC'

const UseReducerhooks = () => {
    return (
        <>
            {/* <CounterOne /> */}
            {/* <CounterTwo /> */}
            {/* <CounterThree message="&lt;3" message1={'<3'} /> */}
            <ComponentABC />
        </>
    )
}

export default UseReducerhooks
