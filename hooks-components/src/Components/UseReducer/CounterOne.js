import React,{useReducer} from 'react'

const initialState = 0

const reducer = (state, action) =>{
    console.log(state, action);
    switch (action) {
        case 'Increment':
            return state + 1

        case 'Decrement':
            return state - 1

        case 'Reset':
            return initialState
    
        default:
            return state;
    }

}

const CounterOne = () =>{

    const [count, dispatch ] = useReducer(reducer, initialState)

    return(
        <>
            <h1>Count : {count}</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>dispatch('Decrement')}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>dispatch('Reset')}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>dispatch('Increment')}>Increment</button>
        </>
    )
}

export default CounterOne




// Simple count


// const CounterOne = () =>{
//     const initialState = 0
//     const [count, setCounter] = useState(0)

//     return(
//         <>
//             <h1>Count - {count}</h1>
//             <button type="button" class="btn btn-outline-danger m-5" onClick={()=>setCounter(preCount => preCount - 1)}>Decrement</button>
//             <button type="button" class="btn btn-outline-secondary m-5" onClick={()=>setCounter(initial)}>Reset</button>
//             <button type="button" class="btn btn-outline-success m-5" onClick={()=>setCounter(preCount => preCount + 1)}>Increment</button>
//         </>
//     )
// }