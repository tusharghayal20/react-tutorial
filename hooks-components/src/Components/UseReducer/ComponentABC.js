import React, {useReducer} from 'react'
import ComponentA from './ComponentA'
import ComponentB from './ComponentB'
import ComponentC from './ComponentC'

export const CounterContext = React.createContext()

const initialState = 0

const reducer = (state, action) =>{
    switch (action) {
        case 'Increment':
            return state + 1

        case 'Decrement':
            return state - 1

        case 'Reset':
            return initialState
    
        default:
            return state;
    }

}

const ComponentABC = () => {
    const [count, dispatch] = useReducer(reducer, initialState)
    const [countTwo, dispatchTwo ] = useReducer(reducer, initialState)
    return (
        <div>
            <h1>Count :- {count}</h1>
            <h1>Count 2:- {countTwo}</h1>
            <CounterContext.Provider value={{ countState: count, countDispatch: dispatch, countTwo, dispatchTwo}}>
                <ComponentA />
                <ComponentB />
                <ComponentC />
            </CounterContext.Provider>
        </div>
    )
}

export default ComponentABC
