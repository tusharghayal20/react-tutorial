import React, { useContext } from 'react'
import { CounterContext } from './ComponentABC'

const ComponentA = () => {
    const countContext = useContext(CounterContext) 
    return (
        <>
            <h1>ComponentA </h1>
            <h1>{ countContext.countState }</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>countContext.countDispatch('Decrement')}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>countContext.countDispatch('Reset')}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>countContext.countDispatch('Increment')}>Increment</button>
        </>
    )
}

export default ComponentA
