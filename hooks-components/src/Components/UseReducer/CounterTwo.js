import React, {useReducer} from 'react'

const initialState = {
    initialCount: 0,
    secondCounter:10
}
console.log("initialState",initialState);
const reducer = (state = initialState, action)=>{
    console.log(state, action)
    switch(action.type){
        case 'Increment':
            return {
                ...state,
                initialCount: state.initialCount + action.value
            }

        case 'Decrement':
            return {
                ...state,
                initialCount: state.initialCount - action.value
            }

        case 'Increment2':
			return { 
                ...state, 
                secondCounter: state.secondCounter + action.value 
            }

		case 'Decrement2':
			return { 
                ...state, 
                secondCounter: state.secondCounter - action.value 
            }

        case 'Reset':
            return initialState

        default:
            return state
    }
}

const CounterTwo = () =>{

    const [ count, dispatch] = useReducer(reducer, initialState)

    return(
        <>
            <h1>Count : {count.initialCount}</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>dispatch({type: 'Decrement', value: 5})}>Decrement 5</button>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>dispatch({type: 'Decrement', value: 1})}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>dispatch({type: 'Reset'})}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>dispatch({type: 'Increment', value: 1})}>Increment</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>dispatch({type: 'Increment', value: 5})}>Increment 5</button>
           
            <h1>secondCounter : {count.secondCounter}</h1>
			<button type="button" className="btn btn-outline-success m-5" onClick={() => dispatch({ type: 'Decrement2', value: 1 })}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>dispatch({type: 'Reset'})}>Reset</button>
			<button type="button" className="btn btn-outline-danger m-5" onClick={() => dispatch({ type: 'Increment2', value: 1 })}>Increment</button>
			
        </>
    )
}

export default CounterTwo;