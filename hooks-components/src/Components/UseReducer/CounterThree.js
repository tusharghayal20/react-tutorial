import React, {useReducer} from 'react'

const initialState = 0

const reducer = (state, action) =>{
    switch (action) {
        case 'Increment':
            return state + 1

        case 'Decrement':
            return state - 1

        case 'Reset':
            return initialState
    
        default:
            return state;
    }

}

const CounterThree = () =>{

    const [count, dispatch ] = useReducer(reducer, initialState)
    const [countTwo, dispatchTwo ] = useReducer(reducer, initialState)

    return(
        <>
            <h1>Two useReducer with same reducer but act as diff</h1>
            <h1>Count 1:- {count}</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>dispatch('Decrement')}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>dispatch('Reset')}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>dispatch('Increment')}>Increment</button>
            <h1>Count 2:- {countTwo}</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>dispatchTwo('Decrement')}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>dispatchTwo('Reset')}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>dispatchTwo('Increment')}>Increment</button>
        </>
    )
}

export default CounterThree