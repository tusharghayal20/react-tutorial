import React, { useContext } from 'react'
import { CounterContext } from './ComponentABC'

const ComponentC = () => {
    const countContext = useContext(CounterContext) 
    return (
        <>
            <h1>ComponentC</h1>
            <h1>{ countContext.countTwo }</h1>
            <button type="button" className="btn btn-outline-danger m-5" onClick={()=>countContext.dispatchTwo('Decrement')}>Decrement</button>
            <button type="button" className="btn btn-outline-secondary m-5" onClick={()=>countContext.dispatchTwo('Reset')}>Reset</button>
            <button type="button" className="btn btn-outline-success m-5" onClick={()=>countContext.dispatchTwo('Increment')}>Increment</button>
        </>
    )
}

export default ComponentC