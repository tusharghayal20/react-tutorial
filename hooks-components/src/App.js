import './App.css';
import Routing from './Components/Routing'
// import ClassCounter from './Components/StateHooks/ClassCounter'
// import HookCounter from './Components/StateHooks/HookCounter'

// import ClassCounterOne from './Components/EffectHooks/ClassCounterOne'
// import HookCounterOne from './Components/EffectHooks/HookCounterOne'

// import ClassMouse from './Components/EffectHooks/ClassMouse'
// import HookMouse from './Components/EffectHooks/HookMouse'

// import IntervalClassCount from './Components/EffectHooks/IntervalClassCount'
// import IntervalHookCount from './Components/EffectHooks/IntervalHookCount'

//  import DataFetching from './Components/EffectHooks/DataFetching'

// import ContextHooks from './Components/ContextHooks/ContextHooks'

// import UseReducerhooks from './Components/UseReducer/UseReducerhooks'

import UseCallback from './Components/useCallback/UseCallback'

// import UseRef from './Components/useRef/UseRef'

// import CustomHook from './Components/CustomHook/CustomHook'

//Hoc
// import HOC from './Components/HOC'
// import RenderProps from './Components/RenderProps'
// import LifeCycleClass from './Components/LifeCycleClass'


import ResizedWindow from './Components/All/ResizedWindow'

import ChildrenProps from './Components/ChildrenProps/ChildrenProps'

function App() {
  return (
    <div>
      <h1>App</h1>
      
      
      
      {/* <Routing /> */}




      {/* *-********  HOOKS ***********/}
       {/* <ClassCounter />
       <HookCounter /> */}

       {/* <ClassCounterOne />
       <HookCounterOne /> */}

       {/* <ClassMouse />
       <HookMouse /> */}

       {/* <IntervalClassCount /> 
       <IntervalHookCount /> */}

       {/* <DataFetching /> */}

       {/* <ContextHooks /> */}

       {/* <UseReducerhooks /> */}

       <UseCallback />

       {/* <UseRef /> */}

       {/* <CustomHook /> */}

        {/* *-********  HOC ***********/}
             {/* <HOC /> */}




        {/* *-********  Render Props ***********/}

        {/* <RenderProps />
        <LifeCycleClass name="Tushar"/> */}

        {/* <ChildrenProps>
          <h1>Children.map</h1>
          <h1>Children.forEach</h1>
          <h1>Children.count</h1>
          <h1>Children.only</h1>
          <h1>Children.toArray</h1>
        </ChildrenProps> */}

          {/********************  ResizedWindow ********************/}
        {/* <ResizedWindow /> */}
    </div>
  );
}

export default App;
