FROM node:15
WORKDIR /app
COPY package.json . (or /app)    (dot point to /app in container)

# If nodemon is not working on windos use -L flag on script

 "scripts": {
    "start": "node index.js",
    "dev": "nodemon -L index.js" 
  },

# To check env variable go to container and run
    printenv     
docker run -v %cd%:/app -p 3000:3000 -d --name app-container app-image
docker run -v %cd%(local path):/app(container path) 
                        -v /app/node_modules (preserve the file)
                                            -p 3000(expose port):3000(container port) 
                                                        -d --name app-container app-image

docker run -v %cd%:/app --env PORT=4000 -p 3000:4000 -d --name app-container app-image
docker run -v %cd%:/app --env-file ./.env(file path) -p 3000:4000 -d --name app-container app-image (For env file )

docker build -t app-image
docker run -v %cd%:/app --env-file ./.env -p 3000:4000 -d --name app-container app-image

# To run docker compose file
    docker-compose up -d (Without -d it will not exit the terminal)

    docker-compose up -d  (will only build image drom cache)

# To create fresh new build
    docker-compose up -d --build


# -----------------------------
# -------  Mongo Db     -------
# -----------------------------

    #Test => mongo -u "root" -p 123456
    #check datrabase => db ,  show dbs
    #aadd entry   db.books.insert({"name": "Harry Potter"})
    #list all doc => db.books.find()

direct 
    docker exec -it {container name / id} bash mongo -u {"user name"} -p {"password"}


# To remove all unused volume
    docker volume prune


# To check/ inspect container
docker inspect {container name / id}

# To see all network
docker network ls

# To run this docker containers
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d --build
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d --build -V //To create new anyomonous volume
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d --scale react-app=2


docker logs node-docker_react-app_1 -f
docker logs node-docker_react-app_2 -f

# To check on container console log
docker logs {container name / id}
# In exec we can ping container name to get ip
ping mongo



# -----------------------------
# -------      ALL      -------
# -----------------------------

1. Create a docker file
2. Run this command and it will create an image from that docker file => docker build -t {Docker image name} .
3. It will create an container => docker run -d --name {container name} {Docker image name} 