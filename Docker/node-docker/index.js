const express = require("express");
const mongoose = require("mongoose");

const redis = require('redis')
const session = require('express-session')

const cors = require("cors")

const { MONGO_USER, MONGO_PASSWORD, MONGO_IP, MONGO_PORT, REDIS_URL, REDIS_PORT,  SESSION_SECRET } = require("./config/config");
let RedisStore = require('connect-redis')(session)
let redisClient = redis.createClient({
    host: REDIS_URL,
    port: REDIS_PORT
})

const postRoutes = require('./routes/postsRoutes')
const userRoutes = require('./routes/usersRoutes')

const app = express(express.json())

// mongoose.connect('mongodb://root:123456@172.20.0.2:27017/?authSource=admin')
                                    // OR we can use container name // It only works on custom network not on default network
const mongoUrl = `mongodb://root:123456@mongo:27017/?authSource=admin` // Docker
// const mongoUrl = `mongodb+srv://Tushar:Tushar123@game-uhx0b.mongodb.net/Test?retryWrites=true&w=majority` // live
// const mongoUrl = `mongoURI":"mongodb://localhost:27017/Test?retryWrites=true&w=majority` // local

// const mongoUrl = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_IP}:${MONGO_PORT}/?authSource=admin`;

mongoose.connect(mongoUrl,{
    useNewUrlParser: true,
    useUnifiedTopology: true })
            .then(()=> console.log("Successfully connected to db"))
            .catch((e)=>console.log(e));

//Init Middleware
app.use(express.json({ extend: false}));

app.enable("trust proxy");
app.enable(cors({}));

app.use(
    session({
      store: new RedisStore({ client: redisClient }),
      secret: SESSION_SECRET,
      cookie: {
          secure: false,
          resave: false,
          saveUninitialized: false,
          httpOnly: true,
          maxAge: 3000000
      }
    })
  )

//Due to default config we have to use /api as we setr /api as an prifix in the config file for NGINX
app.get("/api/hi", (req, res)=>{
    console.log("HHHHHI");
    res.send("<h2>Hi There</h2>");
})

app.use("/api/v1/posts", postRoutes)
app.use("/api/v1/user", userRoutes)

const port = process.env.PORT || 3000

app.listen(port, () => console.log(`listening on port ${port}`));