const Post = require("../models/postModel");
const mongoose = require('mongoose');

exports.getPosts = async (req, res) => {
  try {
      const posts = await Post.find().sort({ _id: -1 });
      console.log("posts",posts);
      res.json({ data:posts });
  } catch (error) {    
      res.status(404).json({ message: error.message });
  }
}

exports.createPost = async (req, res)=>{
  try {
      const post = req.body ;
      console.log("=====>",post);
      const postData = new Post(req.body)
      await postData.save();
      res.status(201).json(postData);
  } catch (error) {
      res.status(409).json({ message: error.message });
  }
}

exports.updatePost = async (req, res) =>{
  //{ id: _id } Rename id to_id
const { id: _id } = req.params;
const post = req.body;
// //Check _id is mongoose object id or not
if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No post with that id')  //404 Not Found
try {
  const updatedPost = await Post.findByIdAndUpdate(_id, post, {new: true})
  res.status(200).json(updatedPost)
} catch (error) {
  res.status(500).json({ message: error.message })
}
}


exports.deletePost = async (req, res) =>{
try {
  const { id: _id } = req.params;
  if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No post with that id') //404 Not Found
  await Post.findByIdAndDelete(_id)
  res.status(200).json({ message: 'Post deleated Successfully'});
} catch (error) {
  res.status(500).json({ message: error.message })
}
}