const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
  title: String,
  message: String,
  name: String,
  creator: String
})

const Post = mongoose.model('Post', postSchema);
module.exports = Post