const express = require("express");
const auth = require('../middelware/auth')

const postController = require("../controllers/postController");

const router = express.Router();

// router.route("/").get(postController.getAllPosts).post(postController.createPost);

router.get('/' , postController.getPosts); // default '/posts'
router.post('/' , postController.createPost);
router.patch('/:id' , postController.updatePost);
// API.patch(`/posts/${id}`, updatedPost);
router.delete('/:id' , postController.deletePost);
// API.delete(`/posts/${id}`, deletePost);


module.exports = router;