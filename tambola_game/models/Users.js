const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    fname: {
        type: String,
        required: true
    },
    lname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        lowercase: true, 
        unique: true, 
        required: [true, "can't be blank"], 
        match: [/\S+@\S+\.\S+/, 'is invalid'], 
        index: true,
        trim: true,
    },
    email_verified:{
        type: Boolean,
        required:true,
        default:true
    },
    password: {
        type: String,
        trim: true,
        required: true
    },
    avatar: {
        type: String
    },
    age: { 
        type: Number, 
        min: 18,
        trim: true, 
        required: true 
    },
    countery_code:{
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true
    },
    phone_verified:{
        type: Boolean,
        required:true,
        default:true
    },
    date: {
        type: Date,
        default: Date.now
    },
    login_count: {
        type:Number,
        default:0
    },
    last_ip: {
        type: String
    },
    last_login: {
        type: String
    },
    last_browser: {
        type: String
    },
    last_location: {
        type: String
    },
    user_type: {
        type: String,
        default:"Admin"
    },
    blocked: {
        type: Boolean,
        // required:true,
        default:false
    }
})

module.exports = User = mongoose.model('user', UserSchema);