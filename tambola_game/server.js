const express = require('express');
const connectDB = require('./config/db')
const app = express();


//Call function connect db
connectDB();

//Init Middleware
app.use(express.json({ extend: false}));

app.get('/', (req, res) => res.send("API Running"));

//Define Routes
app.use('/api/users', require('./routes/api/users'))
app.use('/api/auth', require('./routes/api/auth'))
app.use('/api/profile', require('./routes/api/profile'))
app.use('/api/posts', require('./routes/api/posts'))

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
// mongodb+srv://Tushar:<password>@game-uhx0b.mongodb.net/test?retryWrites=true&w=majority