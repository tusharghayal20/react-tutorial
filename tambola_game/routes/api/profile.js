const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");

const auth = require("../../middleware/auth");
const User = require("../../models/Users");
const Profile = require("../../models/profile");
/*
 * @route  GET api/profile/me
 * @desc   Get current user profile
 * @access Private
 */
router.get("/me", auth, async (req, res) => {
  try {
    const profile = await Profile.findOne({
      user: req.user.id,
    }).populate("user", ["name", "avatar"]);

    if (!profile) {
      return res.status(400).json({ msg: "Their is no profile for user" });
    }

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

/*
 * @route  GET api/profile
 * @desc   Create or Update Profile
 * @access Private
 */

router.post(
  "/",
  [
    auth,
    [
      check("status", "Status is required").not().isEmpty(),

      check("skills", "skills is required").not().isEmpty(),
    ],
  ],

  async (req, res) => {
    // console.log(req.headers['x-auth-token'])

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const {
      status,
      skills,
      company,
      website,
      location,
      bio,
      githubusername,
      youtube,
      facebook,
      twitter,
      instagram,
      linkedin,
    } = req.body;

    profileFields = {};

    //Build profile object
    profileFields.user = req.user.id;
    if (status) profileFields.status = status;
    if (skills) {
      profileFields.skills = skills
        .split(",")
        .map((skill) => " " + skill.trim()); // Put spast so that it is easy to display on frontend
    }
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (location) profileFields.location = location;
    if (bio) profileFields.bio = bio;

    //Build social array
    profileFields.social = {};
    if (githubusername) profileFields.social.githubusername = githubusername;
    if (youtube) profileFields.social.youtube = youtube;
    if (facebook) profileFields.social.facebook = facebook;
    if (twitter) profileFields.social.twitter = twitter;
    if (instagram) profileFields.social.instagram = instagram;
    if (linkedin) profileFields.social.linkedin = linkedin;

    // We can also do that
    /*
profileFields = {
    status: status,
    skills: Array.isArray(skills) ? skills : skills.split(',').map(skill => ' ' + skill.trim()),
    company, // same name so single time
    website: website === '' ? '' : normalize(website, { forceHttps: true }),
    location,
    bio,
    social:{
        githubusername,
        youtube,
        facebook,
        twitter,
        instagram,
        linkedin,
    }
}*/

    try {
      /*
            ////////  This code also work But following code is simple
      let profile = await Profile.findOne({ user: req.user.id });
        
      //Update
      if (profile) {

        profile = await Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields },
          { new: true }
        );

        return res.json(profile);
      }

      // Create Profile
      profile = new Profile(profileFields);

      await profile.save();

      res.json(profile);
      */
      //  It wil update or create profile with option  { upsert: true}
      let profile = await Profile.findOneAndUpdate(
        { user: req.user.id },
        { $set: profileFields },
        { new: true, upsert: true }
      );
      res.json(profile);
    } catch (err) {
      console.error(err.message);
      res.status(400).send("Server Error");
    }
  }
);

/*
 * @route  GET api/profile
 * @desc   Get all Profile
 * @access Private
 */

router.get("/", async (req, res) => {
  try {
    const profiles = await Profile.find().populate("user", ["name", "avatar"]); // if u donot send anything in find it will send all
    res.json(profiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

/*
 * @route  GET api/profile/user/:user_id
 * @desc   Get Profile by user id
 * @access Private
 */

router.get("/user/profile", auth, async (req, res) => {
  try {
    const profiles = await Profile.findOne({
      user: req.user.id,
    }).populate("user", ["name", "avatar"]);

    if (!profiles) {
      return res.status(400).json({ msg: "Profile not found" });
    }

    res.json(profiles);
  } catch (err) {
    console.error(err.message);

    if(err.kind == 'ObjectId'){
        return res.status(400).json({ msg: "Profile not found" });
    }
    res.status(500).send("Server Error");
  }
});

/*
 * @route  GET api/profile
 * @desc   Delete User with Profile, Posts
 * @access Private
 * 
 * http://localhost:5000/api/profile
 * x-auth-token //You have to only provide x auth
 */

router.delete('/', auth, async (req, res) => {
  try {
    
    // Remove Profile
    await Profile.findOneAndRemove({ user: req.user.id });

    // Remove user
    await User.findByIdAndRemove({ _id: req.user.id});

    res.json({ msg: 'User Deleted' });
  } catch (err) {
     console.error(err.message);
     res.status(500).send('Server Error')
  }
})


/*
 * @route  GET api/profile/experience
 * @desc   Add Profile profile
 * @access Private
 */

router.put('/experience', [auth, [
  check('title', 'Title is required').not().isEmpty(),
  
  check('company', 'Company is required').not().isEmpty(),

  check('from', 'From date is required and needs to be from the past')
  .not()
  .isEmpty()
  .custom((value, { req }) => (req.body.to ? value < req.body.to : true)),

  // check('to', 'To is required').not().isEmpty(),

]], 

async (req, res) => {
  
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { 
    title,
    company,
    location,
    from,
    to,
    current,
    description
  } = req.body;

  const newExp = {
    title,
    company,
    location,
    from,
    to,
    current,
    description
  }
  
  try {
    
    const profile = await Profile.findOne({ user: req.user.id })
    
    profile.experience.push(newExp); // add object to profile experience array
    
    await profile.save();
    
    res.json(profile);

  } catch (err) {
     console.error(err.message);
     res.status(500).send('Server Error')
  }

})

/*
 * @route  DELETE api/profile/experience/:exp_id
 * @desc   Delete experience from profile
 * @access Private
 */

router.delete('/experience/:exp_id', auth, async (req, res)=>{
  try {
   const profile = await Profile.findOne({ user: req.user.id })
   
/* //Tushar 
// My logic one line

profile.experience = profile.experience.filter( experience => experience._id.toString() !== req.params.exp_id);
*/   

/**
 *  logic Second
 
  const removeIndex = profile.experience
  .map(item => item.id)
  .indexOf(req.params.exp_id);
    
  profile.experience.splice(removeIndex, 1)
*/

  await profile.save();
  res.json(profile)
   
  } catch (err) {
     console.error(err.message);
     res.status(500).send('Server Error')
  }
}) 


/*
 * @route  PUT api/profile/education
 * @desc   Add Education in profile
 * @access Private
 */
router.put('/education',
 [ auth, [

  check('school', 'school is required').not().isEmpty(),
  
  check('degree', 'degree is required').not().isEmpty(),

  check('fieldofstudy', 'fieldofstudy is required').not().isEmpty(),

  check('from', 'From date is required and needs to be from the past')
  .not()
  .isEmpty()
  .custom((value, { req }) => (req.body.to ? value < req.body.to : true)),

]],

async (req, res) =>{

  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const {
    school,
    degree,
    fieldofstudy,
    from,
    to,
    current,
    description
  } = req.body

  const education =   {
    school,
    degree,
    fieldofstudy,
    from,
    to,
    current,
    description
  }

  try {
    
    const profile = await Profile.findOne({ user: req.user.id });

    profile.education.push(education)

    await profile.save(profile);

    res.json(profile)

  } catch (err) {
     console.error(err.message);
     res.status(500).send('Server Error')
  }
})

/*
 * @route  DELETE api/profile/education/:edu_id
 * @desc   Delete single Education from profile
 * @access Private
 */

router.delete('/education/:edu_id', auth, async (req, res)=>{
  try {

    const profile = await Profile.findOne({ user: req.user.id })

    profile.education = profile.education.filter( education => education._id.toString() !== req.params.edu_id )

    await profile.save()

    res.json(profile)

  } catch (err) {
     console.error(err.message);
     res.status(500).send('Server Error')
  }
})


module.exports = router;