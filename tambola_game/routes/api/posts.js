const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator");

const auth = require("../../middleware/auth");
const User = require("../../models/Users");
const Profile = require("../../models/profile");
const Posts = require("../../models/post");

const checkObjectId = require("../../middleware/checkObjectId");
/*
 * @route  POST api/posts
 * @desc   Create post
 * @access Private
 */

router.post(
  "/",
  [auth, [check("text", "Text is required").not().isEmpty()]],

  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select("-password");

      const newPost = new Posts({
        text: req.body.text,
        fname: user.fname,
        lname: user.lname,
        avatar: user.avatar,
        user: req.user.id,
      });

      const post = await newPost.save();

      res.json(post);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

/*
 * @route  Get api/posts
 * @desc   Get all post
 * @access Private
 */

router.get("/", auth, async (req, res) => {
  try {
    const posts = await Posts.find().sort({ date: -1 });

    res.json(posts);
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

/*
 * @route  Get api/posts/:id
 * @desc   Get post by id
 * @access Private
 */

router.get("/:id", [auth, checkObjectId], async (req, res) => {
  try {
    const post = await Posts.findById(req.params.id);
    /*
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' })  // If we are not using "checkObjectId" middleware validation
        }
*/
    res.json(post);
  } catch (err) {
    console.error(err.message);
    /*
       if (err.kind == 'ObjectId' ) {
           return res.status(404).json({ msg: 'post not found' })  // If we are not using "checkObjectId" middleware validation
       }
*/
    res.status(500).send("Server Error");
  }
});

/*
 * @route  DELETE api/posts/:id
 * @desc   Delete post by id
 * @access Private
 */

router.delete("/:id", [auth, checkObjectId], async (req, res) => {
  try {
    const post = await Posts.findById(req.params.id);
    
    /*
        if (!post) {
            return res.status(404).json({ msg: 'Post not found' })  // If we are not using "checkObjectId" middleware validation
        }
    */
        if (post.user.toString() !== req.user.id ) {
            return res.status(401).json({ msg: 'User not authorize' })
        }

        await post.remove()
        res.json({ msg: 'User not authorized'});
  } catch (err) {
    console.error(err.message);
    /*
       if (err.kind == 'ObjectId' ) {
           return res.status(404).json({ msg: 'post not found' })  // If we are not using "checkObjectId" middleware validation
       }
    console.log(post);
    */
    res.status(500).send("Server Error");
  }
});

/*
 * @route  PUT api/posts/like/:id
 * @desc   Like a post
 * @access Private
 */

 router.put('/like/:id', [auth, checkObjectId], async (req, res)=>{
   try {
    
    const post = await Posts.findById( req.params.id );
    
    //Check if post has already been liked
     //Logic second
    if (post.likes.filter(like => like._id.toString() === req.params.id ).length > 0) {
      return res.status(400).json({ msg: 'Post already liked' });
    }
    
    post.likes.unshift({ user: req.user.id })

    await post.save();

    res.json(post.likes)

   } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error')
   }
 })

/*
 * @route  PUT api/posts/unlike/:id
 * @desc   Like a post
 * @access Private
 */

 router.put('/unlike/:id', [auth, checkObjectId], async (req, res)=>{
   try {
    
    const post = await Posts.findById(req.params.id);

    // Check if the post has already been liked
    if (!post.likes.some((like) => like.user.toString() === req.user.id)) {
      return res.status(400).json({ msg: 'Post has not yet been liked' });
    }

    // remove the like
    post.likes = post.likes.filter(
      ({ user }) => user.toString() !== req.user.id
    );

    await post.save();

    return res.json(post.likes);
   } catch (err) {
      console.error(err.message);
      res.status(500).send('Server Error')
   }
 })

module.exports = router;
