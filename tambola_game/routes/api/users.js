const express = require("express");
const router = express.Router();
const gravatar = require('gravatar');
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const normalize = require('normalize-url');

const User = require("../../models/Users");
/*
 * @route  POST api/user
 * @desc   Register user
 * @access Public
 */
router.post(
  "/",
  [
    check("fname", "First Name is required").not().isEmpty(),

    check("lname", "Last Name is required").not().isEmpty(),

    check("email", "Please include a valid email").isEmail(),

    check("password")
      .isLength({ min: 6 })
      .withMessage("must be at least 6 chars long")
      .matches(/\d/)
      .withMessage("must contain a number")
      .not()
      .isIn(["123456", "password", "god"])
      .withMessage("do not use a common password"),

    check("age").isInt({ gt: 15 }).withMessage("age should be 18 or more"),

    check("countery_code","countery code is required")
    .not().isEmpty(),

    check("phone")
      .isInt()
      .isLength({ min: 10 })
      .withMessage("Invalid phone number"),
  ],

  async (req, res) => {

    const errors = validationResult(req);
    
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { fname, lname, email, password, age, phone, countery_code } = req.body;

    try {
      // See if user exists
      let user = await User.findOne({ email: email });

      if (user) {
        res.status(400).json({ errors: [{ msg: "User already exists" }] });
      }

      // Get user gravatar
      const avatar = normalize(
        gravatar.url(email, {
          s: '200',
          r: 'pg',
          d: 'mm'
        }),
        { forceHttps: true }
      );

      user = new User({
        fname,
        lname,
        email,
        avatar,
        age,
        phone,
        password,
        countery_code
      });

      // Encript password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

        await user.save();
        res.send(user)
      
      // Return jsonwebtoken
      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtToken"),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
