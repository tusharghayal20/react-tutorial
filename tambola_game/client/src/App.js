import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Landing from './components/layout/Landing';
import Navbar from './components/layout/Navbar';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Alert from './components/layout/Alert'
import Dashboard from './components/dashboard/Dashboard'
import CreateProfile from './components/profile-forms/ProfileForm'
import { loadUser } from './store/actions/authAction'
import setAuthToken from './utils/setAuthToken'
//Redux
import { Provider } from 'react-redux'; // this will connect react to redux // react and redux is seperate package
import store from './store/store'
//PrivateRoute
import PrivateRoute from './components/routing/PrivateRoute'
if (localStorage.token) {
  setAuthToken(localStorage.token)
}

const App = () => {

  useEffect(()=>{
    store.dispatch( loadUser() );
  },[])
  
  ///If we were using class component then
  // componentDidmount(){
  //   store.dispatch( loadUser() );
  // }


  return (
    <>
    <Provider store = { store }>
      <Router>
        <Navbar />
        <Route exact path='/' component={ Landing } />
        <div className="container">
          <Alert />
          <Switch>
            
            <Route exact path='/login' component={ Login } />
            <Route exact path='/register' component={ Register } />
            <PrivateRoute exact path='/dashboard' component={ Dashboard } />
            <PrivateRoute exact path='/create-profile' component={ CreateProfile } />
          
          </Switch>
        </div>
      </Router>
      </Provider>
    </>
  );
}

export default App;
