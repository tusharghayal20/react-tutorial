import React from 'react'

function Spinner() {
    return (
        <div style={{ width: '200px', margin: 'auto', display: 'block' }}>
            Loading... ... ...
        </div>
    )
}

export default Spinner
