import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropType from 'prop-types';
import { logout } from '../../store/actions/authAction';
const Navbar = ({ auth: { isAuthenticated, loading }, logout }) => {

	const authLinks = (
		<ul>
			<li>
				<Link to="/dashboard">
					<i className="fa fa-user" aria-hidden="true"></i>
					<span className="hide-sm">Dashboard</span>
				</Link>
			</li>
			<li>
				<a onClick={logout} href="#!">
					<i className="fa fa-sign-out" aria-hidden="true"></i>
					<span className="hide-sm">Logout</span>
				</a>
			</li>
		</ul>
	);

	const guestLinks = (
		<ul>
			<li>
				<Link to="/profiles">Developers</Link>
			</li>
			<li>
				<Link to="/register">Register</Link>
			</li>
			<li>
				<Link to="/login">Login</Link>
			</li>
		</ul>
	);

	return (
		<>
			<nav className="navbar bg-dark">
				<h1>
					<Link to="/dashboard">
						{' '}
						<i className="fa fa-code" aria-hidden="true"></i>Tushar{' '}
						
					</Link>
				</h1>
          {/* { !loading &&     isAuthenticated ? authLinks : guestLinks }  // It will show a difference it is little bit lag */}
		  	  { !loading && (<>{isAuthenticated ? authLinks : guestLinks }</>)}
			</nav>
		</>
	);
};

Navbar.propType = {
	logout: PropType.func.isRequired,
	auth: PropType.object.isRequired
};

const mapStateToProps = (state) => ({
	auth: state.auth
});

export default connect(mapStateToProps, { logout })(Navbar);
