import React, { useState } from "react";
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from "prop-types";
import { login } from '../../store/actions/authAction'
import Input from '../common/input'

const Login = ({ login, isAuthenticated, alerts }) => {

    const [FormData, setFormData] = useState({
        email: "",
        password: ""
    });
    
    const { email, password } = FormData;

    const onChange = (e) => {
        setFormData({
            ...FormData,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = async (e) => {
        e.preventDefault();
        console.log(FormData);
        login(FormData)
    };

    //If login authenticated
    if (isAuthenticated) {
      return <Redirect to='/dashboard' />
    }

  let erroeEmail  = alerts.filter(word => word.param === 'email' );
  let erroepassword = alerts.filter(word => word.param === 'password' );

  return (
    <>
      <h1 className="large text-primary">Sign In</h1>
      <p className="lead">
        <i className="fas fa-user" /> Sign Into Your Account
      </p>
      <form className="form" onSubmit={e => onSubmit(e)}>
        
        <Input 
          type="email"
          placeholder="Email Address"
          name="email"
          value={email}
          onChange={e => onChange(e)}
          autoComplete="username"
          minLength=""
          maxLength=""
          error = {erroeEmail.length > 0 ? erroeEmail[0].msg : null}
          //isRequired = {true}   //default false
          //isDisabled = {true} //default false
        />

        <Input 
          type="password"
          placeholder="Password"
          name="password"
          value={password}
          onChange={e => onChange(e)}
          autoComplete="current-password"
          minLength="6"
          maxLength=""
          error = {erroepassword.length > 0 ? erroepassword[0].msg : null}
          //isRequired = {true}   //default false
          //isDisabled = {true} //default false
        />

        <input type="submit" className="btn btn-primary" value="Login" />
      </form>
      <p className="my-1">
        Don't have an account? <Link to="/register">Sign in</Link>
      </p>
    </>
  );
};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  alerts: PropTypes.array.isRequired,
}

const mapStateToProps = ( state ) =>({
  alerts: state.alert,
  isAuthenticated: state.auth.isAuthenticated,
})

export default connect(mapStateToProps, { login })(Login);
