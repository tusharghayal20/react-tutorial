import React, { useState } from "react";
import { connect } from 'react-redux'
import { Link, Redirect } from "react-router-dom";
import { setAlert } from '../../store/actions/alertAction'
import { register } from '../../store/actions/authAction'
import { dialCode } from '../../utils/dialCode'
import PropTypes from "prop-types";
import Input from '../common/input'
// const Register = (props) => { //we can destructure props
const Register = ({ setAlert, register, alerts, isAuthenticated}) => {
  const [FormData, setFormData] = useState({
    fname: "",
    lname: "",
    email: "",
    age: "",
    country: "",
    countery_code: "",
    phone: "",
    password: "",
    password2: "",
    search: dialCode
  });


  const { fname, lname, email, age, country, phone, countery_code, password, password2, search } = FormData;

  const onChange = (e) => {
    if (e.target.name === "country") {
      let country = dialCode.filter( code => code.name ===  e.target.value);
       setFormData({
        ...FormData,
        country: e.target.value,
        countery_code: country[0].dial_code,
      });
    }

    if(e.target.name !== "country"){
      setFormData({
        ...FormData,
        [e.target.name]: e.target.value,
      });
    }
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (password === password2) {

      register(FormData);

    } else {
      let param="password2"
      // props.setAlert("Password does not match", "danger");
               setAlert("Password does not match", param, "danger", 5000);      // If you distructure
    }
  };

//Display Alert next to Input
let errorfname;
let errorlname;
let errorEmail;
let errorAge;
let errorPhone;
let errorPassword;
let errorPassword2;
if (alerts) {
  errorfname  = alerts.filter(word => word.param === 'lname' );
  errorlname = alerts.filter(word => word.param === 'fname' );
  errorEmail = alerts.filter(word => word.param === 'email' );
  errorAge = alerts.filter(word => word.param === 'age' );
  errorPhone = alerts.filter(word => word.param === 'phone' );
  errorPassword = alerts.filter(word => word.param === 'password' );
  errorPassword2 = alerts.filter(word => word.param === 'password2' );
}

    //If login authenticated
    if (isAuthenticated) {
      return <Redirect to='/dashboard' />
    }
    console.log(alerts);
  return (
    <>
      <section className="container">
        <h1 className="large text-primary">Sign Up</h1>
        <p className="lead">
          <i className="fas fa-user"></i> Create Your Account
        </p>
        <form className="form" onSubmit={(e) => onSubmit(e)}>
            <Input
              type="text"
              placeholder="First Name"
              name="fname"
              value={fname}
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorfname.length > 0 ? errorfname[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />
          {/* { errorfname.length > 0 ? <div style={{color: "red"}}>{fnamealerts[0].msg}</div> : null} */}

            <Input
              type="text"
              placeholder="Last Name"
              name="lname"
              value={lname}
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorlname.length > 0 ? errorlname[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />
          {/* { lnamealerts.length > 0 ? <div style={{color: "red"}}>{lnamealerts[0].msg}</div> : null} */}

            <Input
              type="email"
              name="email"
              placeholder="Email Address"
              value={email}
              autoComplete="username"
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorEmail.length > 0 ? errorEmail[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />
            {/* <small className="form-text">
              This site uses Gravatar, so if you want a profile image, use a
              Gravatar email
            </small> */}
            <Input
              type="number"
              name="age"
              placeholder="Age"
              value={age}
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorAge.length > 0 ? errorAge[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />

          <div className="form-group">
            <label htmlFor=""></label>
            <select className="form-control" value={country} name="country" onChange={(e) => onChange(e)}>
            {
              dialCode.map( (code ) => (
              <option key={code.code}>{code.name}</option>)
              )
            }
            
            </select>
          </div>

          <div className="phone-container">
              <div className="phone-code">
                <Input
                  type="text"
                  name="countery_code"
                  placeholder="Code"
                  value={countery_code}
                  onChange={(e) => onChange(e)}
                  minLength=""
                  maxLength=""
                  //isRequired = {true}   //default false
                  isDisabled = {true} //default false
                />
              </div>
  
              <div className="phone-number">
                <Input
                  type="tel"
                  name="phone"
                  placeholder="Phone number"
                  value={phone}
                  onChange={(e) => onChange(e)}
                  minLength=""
                  maxLength=""
                  error = {errorPhone.length > 0 ? errorPhone[0].msg : null}
                  //isRequired = {true}   //default false
                  //isDisabled = {true} //default false
                />
              </div>
          </div>

          <div className="form-group">
            <Input
              type="password"
              name="password"
              placeholder="Password"
              value={password}
              autoComplete="new-password"
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorPassword.length > 0 ? errorPassword[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />
          </div>
          <div className="form-group">
            <Input
              type="password"
              name="password2"
              placeholder="Confirm Password"
              value={password2}
              autoComplete="new-password"
              onChange={(e) => onChange(e)}
              minLength=""
              maxLength=""
              error = {errorPassword2.length > 0 ? errorPassword2[0].msg : null}
              //isRequired = {true}   //default false
              //isDisabled = {true} //default false
            />
          </div>
          <input type="submit" value="Register" className="btn btn-primary" />
        </form>
        <p className="my-1">
          Already have an account? <Link to="/login">Sign In</Link>
        </p>
      </section>
    </>
  );
};

Register.propTypes = {
  setAlert: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  alerts: PropTypes.array.isRequired,
  isAuthenticated:PropTypes.bool,
}

const mapStateToProps = (state) => ({
  alerts: state.alert,
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps , { setAlert, register })(Register);