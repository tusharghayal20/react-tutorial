import React from 'react'

function input(props) {
    const { type, placeholder, name, value, onChange, autoComplete, error, isRequired, isDisabled, minLength, maxLength} = props
    const divStyle = {
        color: 'red',
      };
    return (
        <>
            <div className="form-group">
                <input
                    type={type}
                    placeholder={placeholder}
                    name={name}
                    value={value}
                    onChange={e => onChange(e)}
                    autoComplete={autoComplete}
                    minLength = {minLength}
                    maxLength = {maxLength}
                    required = {isRequired ? "required" : false}
                    disabled = {isDisabled ? "disabled" : false}
                />
                <small id="helpId" className="text-muted" style={divStyle}>{error}</small>
            </div>
        </>
    )
}

export default input
