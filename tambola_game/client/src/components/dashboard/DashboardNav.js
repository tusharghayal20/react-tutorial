import React from 'react'
import { Link } from 'react-router-dom'
const naveItem = [
    {path: "/edit-profile", name: "Edit Profile"},
    {path: "/add-experience", name: "Add Experience"},
    {path: "/add-education", name: "Add Education"},
]


function DashboardNave() {
    return (
        <div className='dash-buttons'>
            {
                naveItem.map((item, index)=> {return(
                    <Link to={item.path} key={index} className='btn btn-light'>
                        <i className='fa fa-user-circle text-primary' /> {item.name}
                    </Link>
                )})
            }
        </div>
    )
}

export default DashboardNave