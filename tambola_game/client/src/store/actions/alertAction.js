import {v4 as uuid} from "uuid"; 
import { SET_ALERT, REMOVE_ALERT } from '../types';
import { sign } from 'jsonwebtoken';

export const setAlert = (msg, param = null, alertType = "success", timeout = 9000) => dispatch => {
                      console.log(msg, param, alertType, timeout)
                      //{msg: "First Name is required" param: "fname"}
    const id = uuid();
    //console.log("===========>",msg, alertType, id, timeout);
    dispatch({
        type: SET_ALERT,
        payload: { msg, param, alertType, id}
    })

    setTimeout(()=> dispatch({ 
        type: REMOVE_ALERT,
        payload: id
    }), timeout)
}

// export const removeAlert = () => dispatch => {

// }