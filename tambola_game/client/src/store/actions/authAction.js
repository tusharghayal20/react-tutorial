import { setAlert } from '../actions/alertAction';
import {
	REGISTER_SUCCESS,
	REGISTER_FAIL,
	USER_LOADED,
	AUTH_ERROR,
	LOGIN_SUCCESS,
	LOGIN_FAIL,
	LOGOUT,
} from '../types';
import setAuthToken from '../../utils/setAuthToken';
import axios from 'axios';

//Register User
export const register = (body) => async (dispatch) => {
	const config = {
		headers: {
			'Content-Type': 'application/json',
		},
	};

	try {
		const res = await axios.post('/api/users', body, config);

		dispatch({
			type: REGISTER_SUCCESS,
			payload: res.data,
		});

		dispatch(loadUser());
	} catch (err) {
		const errors = err.response.data.errors;
		console.log(errors);

		if (errors) {
			console.log(errors);
			errors.map((error) =>
				dispatch(setAlert(error.msg, error.param, 'danger'))
			);
		}

		dispatch({
			type: REGISTER_FAIL,
		});
	}
};

//Load User
export const loadUser = () => async (dispatch) => {
	if (localStorage.token) {
		setAuthToken(localStorage.token);
	}

	try {
		const res = await axios.get('/api/auth');

		dispatch({
			type: USER_LOADED,
			payload: res.data,
		});
	} catch (err) {
		dispatch({
			type: AUTH_ERROR,
		});
	}
};

export const login = (body) => async (dispatch) => {
	const config = {
		headerd: {
			'Content-Type': 'application/json',
		},
	};

	try {
		const res = await axios.post('api/auth', body, config);

		dispatch({
			type: LOGIN_SUCCESS,
			payload: res.data,
		});

		dispatch(loadUser());
	} catch (err) {
		const errors = err.response.data.errors;
		console.log(errors);

		if (errors) {
			errors.map((error) =>
				dispatch(setAlert(error.msg, error.param, 'danger'))
			);
		}

		dispatch({
			type: LOGIN_FAIL,
		});
	}
};


export const logout = () => dispatch =>{
    dispatch({
        type: LOGOUT
    })
}