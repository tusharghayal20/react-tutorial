import axios from 'axios';
import { setAlert } from '../actions/alertAction';
import { GET_PROFILE, PROFILE_ERROR } from '../types';

// Get current user profile
export const getCurrentProfile = () => async (dispatch) => {
	try {
		const res = await axios.get('./api/profile/me');

		dispatch({
			type: GET_PROFILE,
			payload: res.data,
		});
	} catch (err) {
		dispatch({
			type: PROFILE_ERROR,
			payload: { msg: err.response.statusText, status: err.response.status },
		});
	}
};

//create or update profile

export const createProfile = (formData, history, edit = false) => async ( dispatch ) => {
	console.log(formData, history, edit);
	try {
		const res = await axios.post('/api/profile', formData);
		console.log(res.data);
		dispatch({
			type: GET_PROFILE,
			payload: res.data
		});

		dispatch( setAlert( edit ? "Profile Updated" : "profile Created", null, "success" ))

		if (!edit) {
			history.push('/dashboard')
		}


	} catch (err) {
		const errors = err.response.data.errors;

		if (errors) {
		  errors.forEach(error => dispatch(setAlert(error.msg, null, 'danger')));
		}
	
		dispatch({
		  type: PROFILE_ERROR,
		  payload: { msg: err.response.statusText, status: err.response.status }
		});
	}
}