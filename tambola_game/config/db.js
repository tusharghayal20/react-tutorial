const mongoose = require("mongoose");
const config = require("config");
const db = config.get("mongoURI");

const connectDB = async () => {
  try {
    mongoose.connect(db, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    console.log("MongoDB Connected...");
  } catch (err) {
    console.log(err.message);
    //Exit process with failure
    process.exit(1);
  }
};
//mongodb+srv://Tushar:Tushar123@game-uhx0b.mongodb.net/Game?retryWrites=true&w=majority
// "mongoURI":"mongodb://localhost:27017/Game?retryWrites=true&w=majority",

module.exports = connectDB;
