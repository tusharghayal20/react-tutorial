import React , { useEffect, useState, useCallback } from 'react'
import { connect } from 'react-redux';
import { getProducts } from '../store/action/productAction'
import { addCartItem, getCartItems, } from '../store/action/cartAction'
import MediaCard from './Product'
import Filters from './Filters'
import Cart from './Cart'

const Products = ({products, getProducts, addCartItem, getCartItems, carts}) => {
    const [productsList, setProductsList] = useState({itemsList:""})
    const [filter, setFilter] = useState({size:"", sort:""})
    const [cartList, setCartList] = useState([]);
    
    useEffect(()=>{
        getProducts()
        setProductsList({ itemsList:products})
    },[products])



    useEffect(() => {
        getCartItems(carts.cartItems)
    },[carts.cartItems])




    const onSort = (e) =>{
        if(e.target.value === 'LH'){
            const list = productsList.itemsList.sort((a, b) => a.price > b.price ? 1 : -1 )
            setProductsList({ itemsList:list})
        }
        if(e.target.value === 'HL'){
            const list = productsList.itemsList.sort((a, b) => a.price < b.price ? 1 : -1 )
            setProductsList({ itemsList:list})
        }
        if(e.target.value === 'Resent'){
            const list = productsList.itemsList.sort((a, b) => a._id > b._id ? 1 : -1 )
            setProductsList({ itemsList:list})
        }
    }

    const onSize = (e) =>{
        if (e.target.value) {
            const list = productsList.itemsList.filter((a) => a.availableSizes.includes(e.target.value) )
            setProductsList({ itemsList:list})
        }
    }

    const resetFilter = () =>{
        setFilter({size:"", sort:""})
        setProductsList({ itemsList:products})
    }
    const addToCart = useCallback((item) =>{
        console.log(item);
        if (item.hasOwnProperty('count')) {
            console.log("count is there");
            console.log(item.count);
        }
        if (!item.hasOwnProperty('count')) {
            console.log("add count");
            setCartList([...cartList, {...item, count:1}])
        }
    },[])
    
    console.log(cartList);
    return (
        <div>
            <Filters {...filter} {...productsList} setFilter={setFilter} onSort={onSort} onSize={onSize} resetFilter={resetFilter}/>
            <div className="d-flex">
                <div className="d-flex flex-wrap justify-content-between">
                    {productsList.itemsList && productsList.itemsList.map(prod => <MediaCard prod={prod} key={prod._id} addToCart={addToCart}/>)}
                </div>
                <div className="w-50 px-3 mt-5">
                    <Cart />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) =>{
    return{
        products: state.product.products,
        carts: state.carts,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        getProducts: () => dispatch(getProducts()),
        addCartItem: (id) => dispatch(addCartItem(id)),
        getCartItems: (items) => dispatch(getCartItems(items)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)
