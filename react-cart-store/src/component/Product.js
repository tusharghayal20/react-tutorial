import React from 'react'

const product = (props) => {
    const { _id, image, title, description, availableSizes, price } = props.prod
    return (
        <div className="mt-5">
            <div className="card" style={{width: '18rem'}}>
            <img className="card-img-top" src={image} style={{height:'430px', borderBottom: '1px solid #e5e5e5'}} alt="Card" />
            <div className="card-body">
                <p className="card-text">{availableSizes}</p>
                <p className="card-text">{price} $</p>
                <h5 className="card-title">{title}</h5>
                <p className="card-text">{description.slice(0, 50)}...</p>
                <div className="btn btn-primary" onClick={()=>props.addToCart(props.prod)}>Add to cart</div>
            </div>
            </div>
        </div>
    )
}

export default product
