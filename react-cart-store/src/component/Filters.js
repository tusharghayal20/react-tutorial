import React from 'react'
import Button from '@material-ui/core/Button';
const Filters = (props) => {
    return (
        <div className="mt-5 d-flex align-items-center">
            Filters
            <div className="d-flex ml-5 align-items-center" onChange={(e)=>props.onSort(e)}>
              <label className="mr-3" htmlFor="sort">Sort</label>
              <select name="sort" id="sort">
                <option value="Resent">Resent</option>
                <option value="LH">L - H</option>
                <option value="HL">H - L</option>
              </select>
            </div>
            <div className="d-flex ml-5 align-items-center" onChange={(e)=>props.onSize(e)}>
              <label className="mr-3" htmlFor="sort">Size</label>
              <select name="sort" id="sort">
                <option value="X">X</option>
                <option value="M">M</option>
                <option value="L">L</option>
                <option value="XL">XL</option>
                <option value="XXL">XXL</option>
              </select>
            </div>
            <Button onClick={props.resetFilter} className="ml-3" variant="contained" color="secondary">
              Reset
            </Button>
        </div>
    )
}

export default Filters
