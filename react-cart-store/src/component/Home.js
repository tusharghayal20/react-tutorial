import React from 'react'
import Nave from './nave'
import Products from './Products'
const Home = () => {
    return (
        <div>
            <Nave />
            <Products />
        </div>
    )
}

export default Home
