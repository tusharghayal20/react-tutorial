import { CART_ITEMS, CART_ITEMS_LIST, ADD_CART_ITEMS, REMOVE_CART_ITEMS } from '../types'

const initialState = {
    cartItems: [],
    cartItemList: []
  }
  
  const cartReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_CART_ITEMS: 
        return {
          ...state,
          cartItems:  action.payload
        }

      case CART_ITEMS: 
        return {
          ...state,
          cartItems:  action.payload
        }
  
      default: return state
    }
  }
  
  export default cartReducer