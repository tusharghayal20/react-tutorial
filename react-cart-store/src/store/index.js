import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducer'
import thunk from 'redux-thunk'
// import logger from 'redux-logger'


//const store = createStore(rootReducer, composeWithDevTools())
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

export default store;