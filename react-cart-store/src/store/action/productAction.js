import { GET_PRODUCTS } from '../types'
import data from '../../data.json'

export const getProducts = () => {
    return {
      type: GET_PRODUCTS,
      payload: data.products
    }
  }
