import { CART_ITEMS, CART_ITEMS_LIST,  ADD_CART_ITEMS, REMOVE_CART_ITEMS } from '../types'
import data from '../../data.json'


export const getCartItems = (cartItems) =>{
    console.log("getCartItems",cartItems);

    return{
        type: CART_ITEMS_LIST,
        payload: data
    }
}
export const addCartItem = (id) =>{
    console.log("addCartItem", id);
    return{
        type: ADD_CART_ITEMS,
        payload: id
    }
}

export const removeCartItem = (id) =>{
    return{
        type: REMOVE_CART_ITEMS,
        payload: id
    }
}