import React, { Component } from 'react'
const data = [
    {name:"", age:""},
    {name:"", age:""},
    {name:"", age:""},
    {name:"", age:""},
    {name:"", age:""},
    {name:"", age:""},
    {name:"", age:""}
]
export class Form extends Component { state = {
    cats: [{name:"", age:""}],
    owner: "",
    description: ""
  }
handleChange = (e) => {
    // if (["name", "age"].includes(e.target.className) ) {
    console.log("eeeeeeee",e.target);
    console.log("eeeeeeee => ",e.target.dataset);
    // console.log("eeeeeeee => => => ",e.target.dataset.id);
    
      let cats = [...this.state.cats]
      cats[e.target.id][e.target.name] = e.target.value.toUpperCase()
      data[e.target.id][e.target.name] = e.target.value.toUpperCase()
      this.setState({ cats }, () => console.log(this.state.cats))
    // } else {
    //   this.setState({ [e.target.name]: e.target.value.toUpperCase() })
    // }
  }
addCat = (e) => {
    this.setState((prevState) => ({
      cats: [...prevState.cats, {name:"", age:""}],
    }));
  }
handleSubmit = (e) => { e.preventDefault() }

    render() {
        console.log("data",data);
        
        let {owner, description, cats} = this.state
        return (
          <form onSubmit={this.handleSubmit} onChange={this.handleChange} >
            <label htmlFor="name">Owner</label> 
            <input type="text" name="owner" id="owner" value={owner} />
            <label htmlFor="description">Description</label> 
            <input type="text" name="description" id="description" value={description} />
            <button onClick={this.addCat}>Add new cat</button>
            {
          cats.map((val, idx)=> {
            let catId = `cat-${idx}`, ageId = `age-${idx}`
            return (
              <div key={idx}>
                <label htmlFor={catId}>{`Cat #${idx + 1}`}</label>
                <input
                  type="text"
                  name="name"
                //   data-id={idx}
                  id={idx}
                  value={cats[idx].name} 
                />
                <label htmlFor={ageId}>Age</label>
                <input
                  type="text"
                  name="age"
                //   data-id={idx}
                  id={idx}
                  value={cats[idx].age} 
                />
              </div>
            )
          })
        }
            <input type="submit" value="Submit" /> 
          </form>
        )}
}

export default Form




// import React, { Component } from 'react'

// export class Form extends Component {

//     constructor(){
//         super();
//         this.state = {
//             Owner:"",
//             Description:"",
//             cats: [
//                 {   
//                     name:"",
//                     age: ""
//                 }
//             ]
            
//         }
//     }

//     handleChange = (e) => {
//         console.log(e.target);
//         // if (["name", "age"].includes(e.target.className) ) {
//         //   let cats = [...this.state.cats]
//         //   cats[e.target.dataset.id][e.target.className] = e.target.value.toUpperCase()
//         //   this.setState({ cats }, () => console.log(this.state.cats))
//         // } else {
//           this.setState({ [e.target.name]: e.target.value.toUpperCase() })
//         // }
//       }

//     addCat = (e) =>{
//         console.log(e.target);
        
//         e.preventDefault()
//         this.setState((preveState =>({
//             cats: [...preveState.cats, {name:"", age:""}]
//         })))
//     }

//     handleSubmit = (e) => { e.preventDefault() 
//         console.log(this.state);
        
//     }

//     render() {
//         const { cats } = this.state
//         console.log(this.state);
        
//         return (
//             <div>
//                 <form onSubmit={this.handleSubmit} onChange={this.handleChange}>
//                     <label htmlFor="owner">Owner</label> 
//                     <input type="text" name="owner" id="owner" />
//                     <label htmlFor="description">Description</label> 
//                     <input type="text" name="description" id="description" />

//                     {
//                         cats.map(( value, index) =>{

//                             let catInputName = `cat-${index}`,   ageInputeName = `age-${index}`
//                             return(
//                                <div key={index}>
//                                     <label htmlFor={catInputName}>{`Cat #${index + 1}`}</label>
//                                     <input
//                                     type="text"
//                                     name={cats[index].name}
//                                     data-id={index}
//                                     id={catInputName}
//                                     className="name"
//                                     />
//                                     <label htmlFor={ageInputeName}>Age</label>
//                                     <input
//                                     type="text"
//                                     name="age"
//                                     data-id={index}
//                                     id={ageInputeName}
//                                     className="age"
//                                     />
//                                </div>
//                             )
                            
//                         })
//                     }

//                     <button onClick={this.addCat}>Add new cat</button>
//                     <input type="submit" value="Submit" /> 
//                 </form>
//             </div>
//         )
//     }
// }

// export default Form
