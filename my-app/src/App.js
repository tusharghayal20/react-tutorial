import React, { Component } from 'react';
// import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Form from './DynamicForm/Form';
import Navbar from './Component/Navbar';
import Home from './Component/Home';
import About from './Component/About';
import Services from './Component/Services';
import Blogs from './Component/Blogs';
import Team from './Component/Team';
import Testimonials from './Component/testimonials';
import ContactUs from './Component/ContactUs';
// import Jotto from './Component/Jotto';

// //for testing only
// class App extends Component {
//   // https://github.com/silvestrevivo/testing-counter/blob/master/src/App.test.js
// 	constructor(props) {
//     super(props)
// 		this.state = {
// 			counter: 0,
//       error: false,
// 		};
// 	}

//   increment = () =>{
//     this.setState({counter: this.state.counter + 1, error: false})
//   }
//   decrement = () =>{
//     if (this.state.counter !== 0) {
//       this.setState({counter: this.state.counter - 1})
//     }else{
//       this.setState({error: true})
//     }
//   }

// 	render() {
// 		return (
// 			<>
//         <Jotto />
// 			  <div className="App">
//   			  <div data-testid="component-app">
//     				<h1 data-testid="counter-dispaly">The counter is currently {this.state.counter}</h1>
//     				<button data-testid="increment-button"
//             onClick={()=>this.increment()}
//             >Increment counter</button>
//     				<button data-testid="decrement-button"
//             onClick={()=>this.decrement()}
//             >Decrement counter</button>
//             {this.state.error && <h1 data-testid="warning">It can not be negative</h1>}
//     			</div>
//   			</div>
// 			</>
// 		);
// 	}
// }

function App() {
  return (
     <>
       {/* <Form /> */}
        <Navbar />
        <Home />
        <About />
        <Services />
        <Blogs />
        <Team />
        <Testimonials />
        <ContactUs />
     </>
  );
}

export default App;

/* 
       <Router>
          <Route exact path='/' component={ Landing } />
       <div className="container">
         <Switch>
           
           <Route exact path='/login' component={ Login } />
           <Route exact path='/register' component={ Register } />
         
         </Switch>
       </div>
     </Router> */
