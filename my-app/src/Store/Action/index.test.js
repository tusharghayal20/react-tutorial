import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';
import { configure } from 'enzyme';

import { correctGuess, actionTypes } from './index'

configure({ adapter: new Adapter() });


describe("correctGuess",()=>{

    test("Return an action with type `CORRECT_GUESS` ", ()=>{
        const action = correctGuess();
        expect(action).toEqual({type: actionTypes.CORRECT_GUESS})
    })

})