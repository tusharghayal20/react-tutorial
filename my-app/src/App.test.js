import React from 'react';
// import { render } from '@testing-library/react';
import App from './App';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';

import { configure } from 'enzyme';
configure({ adapter: new Adapter() });

// https://github.com/silvestrevivo/testing-counter/blob/master/src/App.test.js

/**
 * Factory function to create shallow Wraper for App component
 * @function setup
 * @param {object} props - Component props
 * @param {any} state - Initial state
 * @return {shallowWraper}
 */
const setup = (props={}, state = null)=>{
    const wrapper = shallow(<App {...props}/>);
    if(state) wrapper.setState(state)
    return wrapper;
}

const findByTestArr = (wrapper, val) =>{
    return wrapper.find(val);
}

test("Render without error", ()=>{
    const wrapper = setup()
    const appComponent = findByTestArr(wrapper, `[data-testid="component-app"]`)
    
    expect(appComponent.length).toBe(1);

})

// test("Render without error", ()=>{
//     const wrapper = setup()
//     const appComponent = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`)
//     expect(appComponent.length).toBe(1);

// })

// test("Render Increment button without error", ()=>{
//     const wrapper = setup()
//     const appComponent = findByTestArr(wrapper, `[data-testid="increment-button"]`)
//     expect(appComponent.length).toBe(1);

// })

// test("Render Decrement button without error", ()=>{
//     const wrapper = setup()
//     const appComponent = findByTestArr(wrapper, `[data-testid="decrement-button"]`)
//     expect(appComponent.length).toBe(1);
// })

// test("Count start at 0", ()=>{
//     const wrapper = setup()
//     // const appComponent = findByTestArr(wrapper, `[data-testid="increment-button"]`)
//     const initialCount = wrapper.state('counter')
//     expect(initialCount).toBe(0);

// })

// test("Clicking button increment counter", ()=>{
//   const counter = 7
//   const wrapper = setup(null, {counter})

//   //find btn and click
//   const button = findByTestArr(wrapper, `[data-testid="increment-button"]`);
//   button.simulate('click');
//   wrapper.update();

//   const counterDisplay = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`)
//   expect(counterDisplay.text()).toContain( counter + 1)
// })

// test("Clicking button decrement counter", ()=>{
//   const counter = 7
//   const wrapper = setup(null, {counter})

//   //find btn and click
//   const button = findByTestArr(wrapper, `[data-testid="decrement-button"]`);
//   button.simulate('click');
//   wrapper.update();

//   const counterDisplay = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`)
//   expect(counterDisplay.text()).toContain( counter - 1)
//   //OR
//   // expect(counterDisplay.text()).toContain( 6 )
// })


// test("Warning should not get displayed",()=>{
//   const wrapper = setup()
//   const error = findByTestArr(wrapper, `[data-testid="warning"]`)
//   expect(error.exists()).toBeFalsy();
// })

// describe('counter is 0', () => {
//   let counter;
//   let wrapper;
//   let counterDisplay;
//   beforeEach(() => {
//     counter = 0;
//     wrapper = setup(null, { counter });
//     counterDisplay = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`);
//   })

//   test('clicking button decrement', () => {
//     /* Return common code
//     // const counter = 0;
//     // const wrapper = setup(null, { counter });
//     // const counterDisplay = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`);
//     // find a button to click
//     */
//     const button = findByTestArr(wrapper, `[data-testid="decrement-button"]`);
//     button.simulate('click');
//     wrapper.update();

//     //find counterdisplay an equal to 0 and appears a warning message
//     const error = findByTestArr(wrapper, `[data-testid="warning"]`)
//     // console.log(error.debug() ); 
//     expect(wrapper.state().counter).toBeGreaterThanOrEqual(0)
//     expect(counterDisplay.text()).toContain(counter);
//     expect(wrapper.state().error).toBe(true)
//     expect(error.text()).toEqual("It can not be negative");
//   })

//   test('clicking button increment', () => {
//     const warningDisplay = findByTestArr(wrapper, `[data-testid="warning"]`);
//     // find a button to click
//     const button = findByTestArr(wrapper, `[data-testid="increment-button"]`);
//     button.simulate('click');
//     wrapper.update();

//     //find counterdisplay an equal to 1
//     const counterDisplay = findByTestArr(wrapper, `[data-testid="counter-dispaly"]`);
//     expect(counterDisplay.text()).toContain(counter + 1);
//     expect(warningDisplay.exists()).toBeFalsy();
//   })
// })
