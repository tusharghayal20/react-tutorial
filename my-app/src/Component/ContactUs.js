import React from 'react'

function ContactUs() {
    return (
        <div id="contact">
            <div className="row">
                <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2662.2553160406123!2d73.8675528096979!3d18.521513735128966!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bf2e67461101%3A0x828d43bf9d9ee343!2sPune%2C%20Maharashtra!5e0!3m2!1sen!2sin!4v1590150154666!5m2!1sen!2sin" width="750" height="450" frameborder="0"  allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <form >
                        <div className="form-group">
                          <label for=""></label>
                          <input type="text" name="" id="" className="form-control" placeholder="" aria-describedby="helpId"/>
                          <small id="helpId" className="text-muted">Help text</small>
                        </div>
                        <div className="form-group">
                          <label for=""></label>
                          <input type="text" name="" id="" className="form-control" placeholder="" aria-describedby="helpId"/>
                          <small id="helpId" className="text-muted">Help text</small>
                        </div>
                        <div className="form-group">
                          <label for=""></label>
                          <input type="text" name="" id="" className="form-control" placeholder="" aria-describedby="helpId"/>
                          <small id="helpId" className="text-muted">Help text</small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ContactUs
