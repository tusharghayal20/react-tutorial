import React from 'react'

function Navbar() {
    return (
        <div>
            <nav className="navBar">
                <a href="#home">Home</a>
                <a href="#about">About</a>
                <a href="#services">Services</a>
                <a href="#blogs">Blogs</a>
                <a href="#team">Team</a>
                <a href="#testimonials">Testimonials</a>
                <a href="#contact">Contact</a>
            </nav>
        </div>
    )
}

export default Navbar
