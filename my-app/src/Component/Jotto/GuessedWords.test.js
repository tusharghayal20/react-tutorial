import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';

import checkPropTypes from 'check-prop-types';
import GuessedWords from './GuessedWords'

import { configure } from 'enzyme';
import { describe } from 'yargs';
configure({ adapter: new Adapter() });

const defaultProps = {
    guessedWords: [{ guessedWord: "train", letterMatchCount: 3 }]
  };

/**
 * Factory function to create a ShalloWrapper for the Congrats component
 * @function setup
 * @param {object} props - Component props specific for this setup
 * @returns {ShalloWrapper}
 */

 const setup = (props={}, state = null)=>{
    const setupProps = { ...defaultProps, ...props };
    const wrapper = shallow(<GuessedWords {...setupProps}/>);
    if(state) wrapper.setState(state)
    return wrapper;
}

const findByTestArr = (wrapper, val) =>{
    return wrapper.find(val);
}

// test("Render without error", ()=>{
//     const wrapper = setup()
//     const appComponent = findByTestArr(wrapper, `[data-testid="guessed-word"]`)
//     //console.log(appComponent.debug());
//     expect(appComponent.length).toBe(1);
// })

//Grouping
describe('If there are no words guessed', () => {

    let wrapper;
    beforeEach(()=>{
        wrapper = setup({ guessedWord: []})
    })

    test("Render without error", ()=>{
        const appComponent = findByTestArr(wrapper, `[data-testid="component-guessed-words"]`)
        expect(appComponent.length).toBe(1);
    })

    test("render instruction to guess word",()=>{
        const appComponent = findByTestArr(wrapper, `[data-testid="guess-instructions"]`)
        expect(appComponent.text().length()).not.toBe(0);
    })

})

describe('If there are words guessed', () => {
    let wrapper;
    const guessedWords = [
      { guessedWord: "train", letterMatchCount: 3 },
      { guessedWord: "agile", letterMatchCount: 1 },
      { guessedWord: "party", letterMatchCount: 5 }
    ];
  
    beforeEach(() => {
      wrapper = setup({ guessedWords });
    });
  

    test("Render without error", ()=>{
        const appComponent = findByTestArr(wrapper, `[data-testid="component-guessed-words"]`)
        expect(appComponent.length).toBe(1);
    })

    test("render guess word section",()=>{
        const appComponent = findByTestArr(wrapper, `[data-testid="guessed-words"]`)
        expect(appComponent.length()).toBe(1);
    })

    test("correct number of guess word",()=>{
        const appComponent = findByTestArr(wrapper, `[data-testid="guessed-words"]`)
        expect(appComponent.length()).toBe(guessedWords.length);
    })
})






/*****************************
 * 
 * 
 * Check Required props
 * 
 */

/** Check props type require check-prop-types npm  */
export const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(
      component.propTypes,
      conformingProps,
      'props',
      component.name);
    expect(propError).toBeUndefined();
  }

test("does not throw warning with expected props", () => {
    checkProps(GuessedWords, defaultProps);
});