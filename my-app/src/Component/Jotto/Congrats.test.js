import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';
import checkPropTypes from 'check-prop-types';
import Congrats from './Congrats'
import App from '../../App'

import { configure } from 'enzyme';
configure({ adapter: new Adapter() });

const defaultProps = { success: false };

/**
 * Factory function to create a ShalloWrapper for the Congrats component
 * @function setup
 * @param {object} props - Component props specific for this setup
 * @returns {ShalloWrapper}
 */

const setup = (props={}, state = null)=>{
    const setupProps = { ...defaultProps, ...props };
    const wrapper = shallow(<Congrats {...setupProps}/>);
    if(state) wrapper.setState(state)
    return wrapper;
}

const findByTestArr = (wrapper, val) =>{
    return wrapper.find(val);
}

test("Render without error", ()=>{
    const wrapper = setup()
    const appComponent = findByTestArr(wrapper, `[data-testid="component-congrats"]`)
    //console.log(appComponent.debug());
    expect(appComponent.length).toBe(1);
})

test("Render No text when 'success' props is 'false' ", ()=>{
    const wrapper = setup({success: false})
    const appComponent = findByTestArr(wrapper, `[data-testid="component-congrats"]`)
    //console.log(appComponent.debug());
    expect(appComponent.length).toBe(1);
    expect(appComponent.text()).toBe("");
})

test("Render text when 'success' props is 'true' ", ()=>{
    const wrapper = setup({success: true})
    const appComponentMessage = findByTestArr(wrapper, `[data-testid="congrats-message"]`)
    //console.log(appComponentMessage.debug());
    expect(appComponentMessage.length).toBe(1);
    // Or
    expect(appComponentMessage.text()).toContain("Congratulations! You guessed the word.");
})

/** Check props type require check-prop-types npm  */

export const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(
        component.propTypes,
        conformingProps,
        'props',
        component.name);
        expect(propError).toBeUndefined();
}

test("does not throw warning with expected props", () => {
    const expectedProps = { success: false };
    checkProps(Congrats, expectedProps);
});