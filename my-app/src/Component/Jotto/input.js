import React, { Component } from 'react'
import { connect } from 'react-redux'

export class input extends Component {
    
    render() {
        const content = this.props.success ? null :
        (
            <form class="form-inline">
              <input 
                data-testid="input-box" 
                type="email" class="form-control" 
                name="" 
                id="word-guess"  
                placeholder="enter guess"/>
                <button type="submit" 
                    data-testid="submit-button" 
                    class="btn btn-primary">Submit</button>
            </form>
        )
        return (
            <div data-testid="component-input">
                {content}
            </div>
        )
    }
}

const mapStateToProps = ({success}) =>{
    return{
        success
    }
}

export default connect(mapStateToProps)(input)
