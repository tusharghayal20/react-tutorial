import React from 'react'
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, render } from 'enzyme';

import { configure } from 'enzyme';

import { createStore } from "redux";
import rootReducer from "../../Store/Reducer";

import Input from "./input";

configure({ adapter: new Adapter() });


export const storeFactory = (initialState) =>{  
    return createStore(rootReducer, initialState)
}

const setup = (initialState={}) =>{
    const store = storeFactory(initialState);
    const wrapper = shallow(<Input store={store}/>).dive();
    console.log(wrapper.debug());
    return ;
}

const findByTestArr = (wrapper, val) =>{
    return wrapper.find(val);
}


describe("render",()=>{

    describe('word has not beeen gudded', () => {

        let wrapper;
        beforeEach(()=>{
            const initialState = { success: false};
            wrapper = setup(initialState)
        })

        test('render component without error', () => {
            const component = findByTestArr(wrapper, `[data-testid="component-input"]`);
            expect(component.length).toBe(1)
        })

        // test('render input box', () => {
        //     const component = findByTestArr(wrapper, `[data-testid="component-box"]`);
        //     expect(component.length).toBe(1)
        // })

        // test('render submit btn', () => {
        //     const component = findByTestArr(wrapper, `[data-testid="submit-button"]`);
        //     expect(component.length).toBe(1)
        // })
        
    });

    describe('word has beeen gudded', () => {

        test('does not render component without error', () => {
            
        })

        test('does not render input box', () => {
            
        })

        test('does not render submit btn', () => {
            
        })

    });


})

describe("correctGuess",()=>{

    // test('should return default initial state of "false" when no action is passed', () => {
    //     const newState = successReducer(undefined, {}); // this is the initial state definition
    //     expect(newState).toBe(false);
    //   });


})