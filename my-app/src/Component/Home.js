import React from 'react'
import '../App.css';
function home() {
  return (
    <div className="hig" id="home">
      <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
        <ol className="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div className="carousel-inner slider_container">
          <div className="carousel-item active">
            <img src="https://assets.sadhguru.org/mahashivratri/wp-content/uploads/2020/02/Shiva-Wallpaper-shivratri.jpg" className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src="https://149366088.v2.pressablecdn.com/wp-content/uploads/2020/02/Focal-Fossa-wallpaper.jpg" className="d-block w-100" alt="..." />
          </div>
          <div className="carousel-item">
            <img src="https://assets.sadhguru.org/mahashivratri/wp-content/uploads/2020/02/Shiva-Wallpaper-Moon-on-his-head.jpg" className="d-block w-100" alt="..." />
          </div>
        </div>
        <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="sr-only">Previous</span>
        </a>
        <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
      <div className="notis-board">
        <div> a  </div>
        <div> a  </div>
        <div> a  </div>
        <div> a  </div>
        <div> a  </div>
      </div>
    </div>
  )
}

export default home
