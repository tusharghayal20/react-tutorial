// If this was a react applicaation then we can use es6 import
//import Redux from 'Redux'  
//BUT
//We are running this app as a simple nodejas application
const redux = require('redux');
//It prodvide some method
const createStore = redux.createStore;
const combineReducers = redux.combineReducers
const BUY_CAKE = 'BUY_CAKE';
const BUY_ICECREAM = 'BUY_ICECREAM';
/*******************************************
*  ACTION 
*******************************************/
//Action is a simply an objct with type and other property
//Only way your application interact with store
//Carry some information from your app to redux store
//They must have 'Type' property that indicate the type of action being perform
//'Type' property is tipicaly define as a string constants
/*
{
    type: BUY_CAKE,
    info: 'First redux action'
}
*/

/* Action Creator */
//Action creator is a function that return an action
const buyCake = () => {
    return{
        type: BUY_CAKE,
        info: 'BUY_CAKE action'
    }
}

const buyIceCream = () => {
    return{
        type: BUY_ICECREAM,
        info: 'BUY_ICECREAM action'
    }
}

/*******************************************
*  ACTION 
*******************************************/
//Reducer specify how the app state change in response to action sent to the store
//Reducer is a function that accept state and action as an argument and return the next state of the application

const initialState = {
    numOfCakes: 10,
    numOfIceCream: 20
}
//Single Rerdux storew
// const reducer = ( state = initialState, action) =>{
//     switch (action.type) {
//         case BUY_CAKE:
//             return{
//                 ...state,
//                 numOfCakes: state.numOfCakes - 1 
//             }
            
//         case BUY_ICECREAM:
//             return{
//                 ...state,
//                 numOfIceCream: state.numOfIceCream - 1 
//             }
    
//         default:
//             return state
//     }
// }

//Seperate reducer
const cakeReducer = ( state = initialState, action) =>{
    switch (action.type) {
        case BUY_CAKE:
            return{
                ...state,
                numOfCakes: state.numOfCakes - 1 
            }

        default:
            return state
    }
}
const iceCreamReducer = ( state = initialState, action) =>{
    switch (action.type) {
        case BUY_ICECREAM:
            return{
                ...state,
                numOfIceCream: state.numOfIceCream - 1 
            }
    
        default:
            return state
    }
}

/*******************************************
*  Redux Store 
*******************************************/
//One store for the entire application

//Single Reducer
//const store = createStore(reducer) //accept parameter reducer function

//CombineReducer
const rootReducers = combineReducers({
    cake: cakeReducer,
    iceCream: iceCreamReducer
})
const store = createStore(rootReducers) //accept parameter reducer function
// const store = createStore(rootReducers, applyMiddleware( name ))



console.log('Initial state', store.getState());
const unsubscribe = store.subscribe(()=> console.log('Updated store', store.getState())); 
//to update state
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyCake())
store.dispatch(buyIceCream())
unsubscribe()