import CakeContainer from './Componrnts/CakeContainer'
import HooksCakeContainer from './Componrnts/HooksCakeContainer'
import IceCreamContainer from './Componrnts/IceCreamContainer'
import HooksIceCreamContainer from './Componrnts/HooksIceCreamContainer'
import ItemContainer from './Componrnts/ItemContainer'
import UsersContainer from './Componrnts/UsersContainer'
import './App.css';

function App() {
  return (
    <div className="App">
      <CakeContainer />
      <HooksCakeContainer />
      <IceCreamContainer />
      <HooksIceCreamContainer />

      
      <ItemContainer cake />
      <ItemContainer />


      <UsersContainer />
    </div>
  );
}

export default App;
