import axios from 'axios'
import {FETCH_USERS_REQUEST, FETCH_USERS_SUCCESS, FETCH_USERS_FAILURE } from './userTypes'

export const fetchUsersRequest = () => {
    return {
      type: FETCH_USERS_REQUEST
    }
  }
  
  export const fetchUsersSuccess = users => {
    return {
      type: FETCH_USERS_SUCCESS,
      payload: users
    }
  }
  
  export const fetchUsersFailure = error => {
    return {
      type: FETCH_USERS_FAILURE,
      payload: error
    }
  }

  export const fetchUsers = () => {
    return function(dispatch){
      dispatch(fetchUsersRequest())
      //To ser loading to true
      axios.get('https://jsonplaceholder.typicode.com/users') // async await not required for then cache
      .then(response =>{
        const users = response.data
        dispatch(fetchUsersSuccess(users))
      }).catch(error=>{
        const errorMsg = error.message
        dispatch(fetchUsersFailure(errorMsg))
      })
    }
  }

  //>>>>>>>>>>>>>>>>>>>>  ERROR

  // export const fetchUsers = () => async dispatch =>{
  //   try {
  //     dispatch(fetchUsersRequest())
  //     const { data } = await axios.get('https://jsonplaceholder.typicode.com/users')
  //     if (data) {
  //       dispatch(fetchUsersSuccess(data))
  //     }
  //   } catch (error) {
  //     dispatch(fetchUsersFailure(error.message))
  //   }
  // }