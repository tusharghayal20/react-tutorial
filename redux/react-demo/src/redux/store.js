import { createStore, applyMiddleware, compose } from 'redux';
// import cakeReducer from './cakes/cakeReducer'
import rootReducer from './rootReducer'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

//const store = createStore(cakeReducer)
const store = createStore(rootReducer, compose(applyMiddleware(logger, thunk)))
// const store = createStore(
//     cakeReducer,
//     compose(applyMiddleware(logger, thunk))
//   )

export default store;