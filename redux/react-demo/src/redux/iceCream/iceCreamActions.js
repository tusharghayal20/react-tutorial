import { BUY_ICECREAM } from './iceCreamTypes'


//* When we use mapStateToDispatch */
export const buyIceCream = () =>  {
    return{
        type: BUY_ICECREAM,  // We are only returning here we can't add api call here
        payload: 1
    }
}

// //* to work this we need redux-thunk as anmiddleware
// export const buyIceCream = () =>  (dispatch) => { 
//     dispatch({
//         type: BUY_ICECREAM,  // we can call api call here with help on redux-thunk
//         payload: 1
//     })
// }

