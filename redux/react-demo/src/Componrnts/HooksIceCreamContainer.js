import React from 'react'
import { connect, useSelector, useDispatch } from 'react-redux'
import { buyIceCream } from '../redux'
import Button from 'react-bootstrap/Button';

const HooksIceCreamContainer = (props) => {
    const numOfIceCream = useSelector(state => state.iceCream.numOfIceCream);
    const dispatch = useDispatch()
    return (
        <>
            <h1>Ice Cream Container</h1>
            <h2>CakeContainer : Number of Ice Cream - {numOfIceCream}</h2>
            <div>For this btn you don't need connect and pass it to function as function</div>
            <Button variant={numOfIceCream !== 0 ? "success" : "danger"} onClick={()=>dispatch(buyIceCream())} disabled={numOfIceCream === 0}>Buy IceCream</Button>
            <div>For this btn you need connect only pass it as an props</div>
            <div>
                    someAction is automatically dispatched for you
                    there is no need to call dispatch(props.buyIceCream());
            </div>
            <div>Note:- however, that dispatch is not available on props if you do pass in actions to your connect function. 
                In other words, in the code, since we are passing buyIceCream action to connect, dispatch() is no longer available on props.</div>
            <Button variant={numOfIceCream !== 0 ? "success" : "danger"} onClick={props.buyIceCream} disabled={numOfIceCream === 0}>Buy IceCream Without dispatch</Button>
        </>
    )
}

export default connect(null, {buyIceCream})(HooksIceCreamContainer);

//Note:- however, that dispatch is not available on props if you do pass in actions to your connect function. 
//In other words, in the code, since we are passing buyIceCream action to connect, dispatch() is no longer available on props.