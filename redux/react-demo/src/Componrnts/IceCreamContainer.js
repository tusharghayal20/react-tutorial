import React from 'react'
import { connect } from 'react-redux'
import { buyIceCream } from '../redux'
import Button from 'react-bootstrap/Button';

const IceCreamContainer = (props) => {
    return (
        <>
            <h1>Ice Cream Container</h1>
            <h2>CakeContainer : Number of Ice Cream - {props.numOfIceCream}</h2>
            <Button variant={props.numOfIceCream !== 0 ? "success" : "danger"} onClick={props.buyIceCream} disabled={props.numOfIceCream === 0}>Buy IceCream</Button>
        </>
    )
}

const mapStateToProps = (state) =>{
    return{
        numOfIceCream: state.iceCream.numOfIceCream
    }
}

///* If we did not use this we have to use thunk as an middleware
// const mapDispatchToProps = (dispatch) =>{
//     return{
//         buyIceCream: ()=>dispatch(buyIceCream())
//     }
// }

export default connect(mapStateToProps, {buyIceCream})(IceCreamContainer)
