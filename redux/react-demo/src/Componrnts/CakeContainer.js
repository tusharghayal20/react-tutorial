import React from 'react';
import { connect } from 'react-redux'
import { buyCake } from '../redux'
import Button from 'react-bootstrap/Button';
function CakeContainer(props) {
    return (
        <>
            <h2>CakeContainer : Number of cakes - {props.numOfCakes}</h2>
            <Button variant={props.numOfCakes !== 0 ? "success" : "danger"} onClick={props.buyCake} disabled={props.numOfCakes === 0}>Buy Cake</Button>
        </>
    )
}

const mapStateToProps = (state) =>{
    return{
        numOfCakes: state.cake.numOfCakes,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        buyCake: () => dispatch(buyCake())
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CakeContainer)
