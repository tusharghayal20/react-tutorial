import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import {fetchUsers} from '../redux'

function UsersContainer({userData, fetchUsers}) {

    useEffect(()=>{
        fetchUsers()
    },[])

    return userData.loading ?(<h2> Loading ...</h2>) : userData.error 
                                                        ? (<h2>{userData.error}</h2>) :
     (
        <>
            <h1> Users Container </h1>
            {
                userData && 
                userData.users  && 
                userData.users.map(user => <p>{user.name}</p>)
            }
        </>
    )
}

const mapStateToProps = (state) =>{
    return{
        userData: state.user
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        fetchUsers: fetchUsers
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer)

// export default connect(mapStateToProps, {fetchUsers})(UsersContainer) // It also works with 

  // export const fetchUsers = () => async dispatch =>{
  //   try {
  //     dispatch(fetchUsersRequest())
  //     const { data } = await axios.get('https://jsonplaceholder.typicode.com/users')
  //     if (data) {
  //       dispatch(fetchUsersSuccess(data))
  //     }
  //   } catch (error) {
  //     dispatch(fetchUsersFailure(error.message))
  //   }
  // }

