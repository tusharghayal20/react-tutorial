import React from 'react'
import Button from 'react-bootstrap/Button';
import { useSelector, useDispatch } from 'react-redux'
import { buyCake } from '../redux'

function HooksCakeContainer() {
    const numOfCakes = useSelector( state => state.cake.numOfCakes)
    const dispatch = useDispatch()
    return (
        <>
            <h2>HooksCakeContainer :  Number of cakes - {numOfCakes}</h2>
            <Button variant={numOfCakes !== 0 ? "success" : "danger"} onClick={()=>dispatch(buyCake())} disabled={numOfCakes === 0}>Buy Cake</Button>
        </>
    )
}

export default HooksCakeContainer // We don't need connect function for react redux hooks
