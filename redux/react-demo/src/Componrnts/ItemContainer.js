import React from 'react'
import { connect } from 'react-redux'
import { buyCake, buyIceCream } from '../redux'
import Button from 'react-bootstrap/Button';

function ItemContainer(props) {
    console.log(props);
    return (
        <div>
            <h1>Item Container</h1>
            <h2>Item: {props.item}</h2>                                                                                    {/* From onProips                                */}
            <Button variant={props.item !== 0 ? "success" : "danger"} onClick={props.buyItems} disabled={props.item === 0}>Buy {props.cake ? 'Cake' : 'IceCream'}</Button>
        </div>
    )
}

const mapStateToProps = (state, ownProps) =>{ // onProps come from App.js
    const itemState = ownProps.cake ? state.cake.numOfCakes : state.iceCream.numOfIceCream
    return{
        item: itemState,
        ownProps: ownProps,
    }
}

const mapDispatchToProps = (dispatch, ownProps) =>{
    const dispatchFunction = ownProps.cake ? () => dispatch(buyCake()) : ()=>dispatch(buyIceCream())
    return{
        buyItems: dispatchFunction
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemContainer)
